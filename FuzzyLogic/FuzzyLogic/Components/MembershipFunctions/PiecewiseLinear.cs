﻿using FuzzyLogic.Core;
using System;

namespace FuzzyLogic.Components.MembershipFunctions
{
    public class PiecewiseLinear : MembershipFunction
    {
        protected PointD[] Points;

        public override double Start => Points[0].X;
        public override double End => Points[Points.Length - 1].X;

        protected PiecewiseLinear(int pts)
        {
            if (pts == 0)
                throw new Exception("Failed to create a membership function: points amount should be positive.");

            Points = new PointD[pts];
            Scale = 1;
        }

        public PiecewiseLinear(params PointD[] pts)
        {
            Points = pts;

            for (var i = 0; i < Points.Length; i++)
            {
                if ((Points[i].Y < 0) || (Points[i].Y > 1))
                    throw new ArgumentException(
                        "Membership functions cannot contain points outside [0 1] range. " +
                        $"Received {Points[i].Y}");

                if (i == 0)
                    continue;

                if (Points[i - 1].X > Points[i].X)
                    throw new ArgumentException("Points' array must be sorted by ascending X.");
            }
            Scale = 1;
        }

        public override double GetMembership(double x)
        {
            if (x < Points[0].X * Scale)
                return Points[0].Y;

            if (x > Points[Points.Length - 1].X * Scale)
                return Points[Points.Length - 1].Y;

            for (int i = 1, n = Points.Length; i < n; i++)
            {
                if (!(x < Points[i].X)) continue;

                var y1 = Points[i].Y;
                var y0 = Points[i - 1].Y;
                var x1 = Points[i].X * Scale;
                var x0 = Points[i - 1].X * Scale;
                var m = (y1 - y0) / (x1 - x0);
                return m * (x - x0) + y0;
            }

            return 0; //should never be reached
        }

        public override PointD[] GetShape()
        {
            var shift = (End - Start) * (ShapeRangeModifier - 1);

            var pts = new PointD[Points.Length + 2];
            Points.CopyTo(pts, 1);

            var pt = Start - shift;
            pts[0] = new PointD(pt, GetMembership(pt));

            pt = End + shift;
            pts[pts.Length - 1] = new PointD(pt, GetMembership(pt));
            for (var i = 0; i < pts.Length; i++)
                pts[i].X *= Scale;

            return pts;
        }
    }
}