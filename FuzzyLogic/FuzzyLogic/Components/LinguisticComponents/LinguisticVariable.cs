﻿using System.Linq;
using FuzzyLogic.Components.MembershipFunctions;

namespace FuzzyLogic.Components
{
    public class LinguisticVariable
    {
        public string Name;
        public LinguisticValue[] FuzzyRange;

        public double Scale
        {
            get => FuzzyRange.First().Value.Scale;
            set
            {
                foreach (var lv in FuzzyRange)
                    lv.Value.Scale = value;
            }
        }

        public double Input;

        public LinguisticVariable(string name)
        {
            Name = name;
            Input = 0;
        }

        public LinguisticVariable(string name, params LinguisticValue[] fuzzyrange) : this(name)
        {
            FuzzyRange = fuzzyrange;
        }

        public LinguisticVariable(string name, params IMembershipFunction[] fuzzyrange) : this(name)
        {
            FuzzyRange = new LinguisticValue[fuzzyrange.Length];
            for (var i = 0; i < FuzzyRange.Length; i++)
                FuzzyRange[i] = new LinguisticValue(name + '.' + i.ToString(), fuzzyrange[i]);
        }

        /// <summary>
        /// Returns an array of membership values that LinguisticValues return given the current Input value.
        /// </summary>
        /// <returns></returns>
        public double[] GetMemberships()
        {
            var ans = new double[FuzzyRange.Length];
            for (var i = 0; i < ans.Length; i++)
                ans[i] = FuzzyRange[i].GetMembership(Input);

            return ans;
        }

        /// <summary>
        /// Returns an array of membership values that LinguisticValues return given an input.
        /// Does not change state of the LinguisticVariable's Input value.
        /// </summary>
        public double[] GetMemberships(double value)
        {
            var ans = new double[FuzzyRange.Length];
            for (var i = 0; i < ans.Length; i++)
                ans[i] = FuzzyRange[i].GetMembership(value);

            return ans;
        }
    }
}