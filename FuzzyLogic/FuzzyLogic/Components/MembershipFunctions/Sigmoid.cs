﻿using System;

namespace FuzzyLogic.Components.MembershipFunctions
{
    public class Sigmoid : MembershipFunction
    {
        public override double Start => Scale * (Middle - _step) - 1.0 / DisplayPointsAmount;
        public override double End => Scale * (Middle + _step) + 1.0 / DisplayPointsAmount;

        public enum Side
        {
            Left,
            Right
        }

        public double Middle { get; } = 0;
        public double InversedCurvature { get; }

        private readonly double _sign = 1;
        private readonly double _step;

        /// <summary>
        /// A sigmoid logistic function.
        /// </summary>
        /// <param name="middle">Middle of the slope</param>
        /// <param name="inversedCurvature">A positive value. Higher it is, smoother is the slope.</param>
        /// <param name="side"></param>
        /// <param name="step">Half of the shape's width to be displayed.</param>
        public Sigmoid(double middle, double inversedCurvature, Side side, double step = 1)
        {
            Middle = middle;
            InversedCurvature = Math.Abs(inversedCurvature);
            InversedCurvature = Math.Min(double.MaxValue, InversedCurvature);

            //Flips a function horizontally
            if (side == Side.Left)
                _sign = -1;

            Scale = 1;

            _step = step;
        }

        public override double GetMembership(double x)
        {
            var sBias = Scale * Middle;
            var sCurve = InversedCurvature * Scale;
            var res = 1 / (1 + Math.Exp(-_sign * (x - sBias) * sCurve));
            return res;
        }
    }
}