﻿using FuzzyLogic.Components;
using FuzzyLogic.Components.Expressions;
using FuzzyLogic.Components.MembershipFunctions;
using Plotter;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuzzyPackage
{
    public partial class Tester
    {
        public void ExpressionTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            FuzzyBuilder builder = new FuzzyBuilder(fuzziness: 1, open: false);

            const int aStart = 0, aEnd = 10, aAmount = 5;
            const int bStart = 1, bEnd = 9, bAmount = 3;

            var a = builder.MakeLinguisticVariable("A", aStart, aEnd, aAmount);
            Logger.WriteLine($"Built Variable A in range [{aStart} {aEnd}] with {aAmount} membership functions.");
            var b = builder.MakeLinguisticVariable("B", bStart, bEnd, bAmount);
            Logger.WriteLine($"Built Variable B in range [{bStart} {bEnd}] with {bAmount} membership functions.");

            var indexA = 1;
            var indexB = 0;
            var isA = new ExpressionIs(a, a.FuzzyRange[indexA]);
            var isB = new ExpressionIs(b, b.FuzzyRange[indexB]);

            var customRule = isA.Not().And(isB.Not());
            Logger.WriteLine($"Custom rule 1. " +
                             $"\nExpected: NOT A is A{indexA} AND NOT B is B{indexB}. " +
                             $"\nReceived: {customRule.Name}");

            var customRule1 = isA.Or(isB).Not();
            Logger.WriteLine($"Custom rule 2. " +
                             $"\nExpected: NOT (A is A{indexA} OR B is B{indexB}). " +
                             $"\nReceived: {customRule1.Name}");

            Logger.WriteLine("Custom rules must give identical memberships due to De Morgan's laws.");

            DisplayFuzzyRange(a);
            DisplayFuzzyRange(b);

            var form = new ChartForm("Custom Rule Membership");
            var start = Math.Min(aStart, bStart);
            var end = Math.Max(aEnd, bEnd);
            var step = 1.0 * (end - start) / Points;

            for (var i = 0; i < Points; i++)
            {
                var input = start + step * i;
                a.Input = input;
                b.Input = input;
                var output = customRule.Value;
                form.WriteData(input, output, "Rule 1");
                var output1 = customRule1.Value;
                form.WriteData(input, output1, "Rule 2");

                form.WriteData(input, isA.Value, $"A is A{indexA}");
                form.WriteData(input, isB.Value, $"B is B{indexB}");
            }

            form.Display();
        }

        /// <summary>
        /// Test MembershipFunction.Join.
        /// </summary>
        public void JoinedMembershipTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            const int mid = 0;
            const int halfwidth = 1;
            const int mid1 = 3;
            const int halfwidth1 = 3;
            const int mid2 = 5;
            const int halfwidth2 = 2;
            const int lim = 10;

            var builder = new MembershipBuilder(1);

            var list = new List<IMembershipFunction>();

            var mem = builder.Build(mid, halfwidth, MembershipFunctionType.Trapezoid, mid - lim, mid + lim);
            var mem1 = builder.Build(mid1, halfwidth1, MembershipFunctionType.Trapezoid, mid1 - lim, mid1 + lim);
            var mem2 = builder.Build(mid2, halfwidth2, MembershipFunctionType.Trapezoid, mid2 - lim, mid2 + lim)
                .Cap(0.5);

            var joined = MembershipFunction.Join(mem, mem1);
            var joined1 = MembershipFunction.Join(mem1, mem2);
            var joined2 = MembershipFunction.Join(joined1, joined, mem);
            joined2 = MembershipFunction.Join(joined2, mem);

            var form = new ChartForm("JoinedMembershipTest");
            form.WriteData(PointDtoPointF(mem.GetShape()), "MF 1");
            form.WriteData(PointDtoPointF(mem1.GetShape()), "MF 2");
            form.WriteData(PointDtoPointF(mem2.GetShape()), "MF 3");
            form.WriteData(PointDtoPointF(joined.GetShape()), "MF1 and MF2");
            form.WriteData(PointDtoPointF(joined1.GetShape()), "MF2 and MF3");
            form.WriteData(PointDtoPointF(joined2.GetShape()), "Previous two joined");
            form.Display();
        }

        /// <summary>
        /// Test 'MembershipFunction.Cap'
        /// </summary>
        public void CappedMembershipTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            const int mid = 0;
            const int halfwidth = 1;
            const int lim = 10;

            var builder = new MembershipBuilder(1);

            var mem = builder.Build(mid, halfwidth, MembershipFunctionType.Trapezoid, mid - lim, mid + lim);
            var list = new List<IMembershipFunction>();
            list.Add(mem);
            list.Add(list[0].Cap(1));
            list.Add(list[1].Cap(0.5));
            list.Add(list[2].Cap(2));
            list.Add(list[3].Cap(0.1));

            var names = new List<string>();
            names.Add("Base");
            names.Add("Cap = 1");
            names.Add("Cap = 0.5");
            names.Add("Cap = 2");
            names.Add("Cap = 0.1");

            var form = new ChartForm("CappedMembershipTest");
            var i = 0;
            foreach (var mf in list)
            {
                var pts = PointDtoPointF(mf.GetShape());

                var type = mf.GetType().ToString();
                type = type.Split('.').Last();

                form.WriteData(pts, names[i] + " : type " + type);
                ++i;
            }

            form.Display();
        }

        /// <summary>
        /// Test how MembershipBuilder builds functions.
        /// </summary>
        /// <param name="step">Distance between midpoints of membership functions.</param>
        /// <param name="halfwidth">Halved width of membership functions.</param>
        /// <param name="fuzziness">Fuzziness of the builder.</param>
        public void MembershipBuilderTest(double step = 4, double halfwidth = 1, double fuzziness = 0.5)
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var builder = new MembershipBuilder(fuzziness);
            var list = new List<IMembershipFunction>();
            var types = new Queue<MembershipFunctionType>();

            //To test and display all builder options.
            var i = 0;
            foreach (MembershipFunctionType type in Enum.GetValues(typeof(MembershipFunctionType)))
            {
                var mid = i * step;
                var min = mid - step * halfwidth;
                var max = mid + step * halfwidth;
                list.Add(builder.Build(mid, halfwidth, type, min, max));
                types.Enqueue(type);

                ++i;
            }

            var form1 = new ChartForm("MembershipBuilder shapes");
            foreach (var item in list)
            {
                var pts = PointDtoPointF(item.GetShape());
                var dataName = types.Dequeue();

                form1.WriteData(pts, dataName.ToString());
            }

            form1.Display();
        }
    }
}