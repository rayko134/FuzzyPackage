﻿using System.Collections.Generic;

namespace FuzzyLogic.Core
{
    public class CircularList<T>
    {
        public int Count => _array.Count;
        public T Current { get; private set; }

        public T First => _array[0];

        public T Last => _array[Count - 1];

        private int _currentIndex;
        private readonly List<T> _array;

        public CircularList()
        {
            _array = new List<T>();
            _currentIndex = -1;
        }

        public CircularList(params T[] array)
        {
            _array = new List<T>();
            _array.AddRange(array);
            _currentIndex = 0;
            Current = _array[_currentIndex];
        }

        /// <summary>
        /// Increments current element.
        /// </summary>
        /// <returns>true when start is reached from the end.</returns>
        public bool Next()
        {
            var res = (_currentIndex == Count - 1);
            _currentIndex = (_currentIndex + 1) % Count;
            Current = _array[_currentIndex];
            return res;
        }

        /// <summary>
        /// Decrements current element.
        /// </summary>
        /// <returns>true when end is reached from the start.</returns>
        public bool Previous()
        {
            var res = (_currentIndex == 0);
            _currentIndex = (_currentIndex + Count - 1) % Count;
            Current = _array[_currentIndex];
            return res;
        }

        public bool IsFirst()
        {
            return (_currentIndex == 0);
        }

        public bool IsLast()
        {
            return (_currentIndex == Count - 1);
        }

        public void Add(T item)
        {
            AddRange(item);
        }

        public void AddRange(params T[] items)
        {
            _array.AddRange(items);
            if (_currentIndex != -1) return;

            _currentIndex = 0;
            Current = _array[_currentIndex];
        }
    }
}