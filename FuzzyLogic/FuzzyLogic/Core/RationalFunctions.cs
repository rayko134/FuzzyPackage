﻿using System;
using System.Linq;
using System.Numerics;

namespace FuzzyLogic.Core
{
    public struct Polynomial
    {
        public static Polynomial One => new Polynomial(1);

        //coefficients from 0 to N, N - constant term, 0 - coefficient at x^(n = N - 1).
        private readonly double[] _coeffs;

        public double this[int i]
        {
            get => _coeffs[i];
            set => _coeffs[i] = value;
        }

        public int Length => _coeffs.Length;

        public Polynomial(params double[] a)
        {
            _coeffs = a;
        }

        public Polynomial(PointD p1, PointD p2)
        {
            _coeffs = new double[2];

            _coeffs[0] = (p2.Y - p1.Y) / (p2.X - p1.X); //K
            _coeffs[1] = -(p1.X * p2.Y - p2.X * p1.Y) / (p2.X - p1.X); //B
        }

        public double Compute(double input)
        {
            double res = 0;
            for (var i = 0; i < _coeffs.Length - 1; i++)
                res = res * input + _coeffs[i];
            res = res * input + _coeffs.Last();
            return res;
        }

        public Complex Compute(Complex input)
        {
            Complex res = 0;
            for (var i = 0; i < _coeffs.Length - 1; i++)
                res = res * input + _coeffs[i];
            res = res * input + _coeffs.Last();
            return res;
        }

        /// <summary>
        /// Gets an analytical derivative
        /// </summary>
        /// <returns></returns>
        public Polynomial GetDerivative()
        {
            var a = new double[_coeffs.Length - 1];
            for (var i = 1; i <= a.Length; i++)
            {
                a[a.Length - i] = _coeffs[a.Length - i] * i;
            }
            return new Polynomial(a);
        }

        /// <summary>
        /// Computes a value Y of the analytical derivative at given X
        /// </summary>
        /// <returns></returns>
        public double GetDerivative(double input)
        {
            return GetDerivative().Compute(input);
        }

        /// <summary>
        /// Computes a scaled polynomial.
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        /// <returns></returns>
        public Polynomial GetScaled(double scaleX, double scaleY)
        {
            var res = new Polynomial(_coeffs);
            for (var i = 0; i < _coeffs.Length - 1; i++)
            {
                res._coeffs[i] *= (scaleY / scaleX);
            }
            res._coeffs[res._coeffs.Length - 1] *= scaleY;

            return res;
        }

        public double[] ToDoubles()
        {
            return _coeffs;
        }

        public float[] ToSingles()
        {
            var res = new float[_coeffs.Length];
            for (var i = 0; i < _coeffs.Length; i++)
                res[i] = Convert.ToSingle(_coeffs[i]);
            return res;
        }

        /// <summary>
        /// Modifies this polynomial so that it crosses polynomial 'pl' at abscissa 'x'
        /// </summary>
        public Polynomial AdjustTo(Polynomial pl, double x)
        {
            var res = new Polynomial(_coeffs);
            res[_coeffs.Length - 1] = pl.Compute(x) - CutConst().Compute(x);
            return res;
        }

        private Polynomial CutConst()
        {
            var pl = new Polynomial(_coeffs);
            if (pl.Length != 1)
                pl._coeffs[_coeffs.Length - 1] = 0;
            else
                pl = new Polynomial(0);
            return pl;
        }

        public bool Equals(Polynomial other)
        {
            return Equals(_coeffs, other._coeffs);
        }

        public override string ToString()
        {
            var res = "";

            for (var i = 0; i < Length - 1; i++)
            {
                res += $"{this[i]}x^{Length - i - 1} + ";
            }

            res += this[Length - 1].ToString();
            return res;
        }

        #region operator +

        public static Polynomial operator +(Polynomial a, Polynomial b)
        {
            var maxL = Math.Max(a.Length, b.Length);
            var res = new double[maxL];
            for (var i = 0; i < a.Length; i++)
            {
                for (var j = 0; j < b.Length; j++)
                {
                    res[b.Length - j - 1] += b._coeffs[b.Length - j - 1];
                }
                res[a.Length - i - 1] += a._coeffs[a.Length - i - 1];
            }
            return new Polynomial(res);
        }

        public static Polynomial operator +(double a, Polynomial b)
        {
            var res = new Polynomial(b._coeffs);
            res._coeffs[0] += a;
            return res;
        }

        public static Polynomial operator +(Polynomial a, double b)
        {
            return (b + a);
        }

        #endregion operator +

        #region operator -

        public static Polynomial operator -(Polynomial a)
        {
            var newCoeff = new double[a.Length];
            for (var i = 0; i < a.Length; i++)
            {
                newCoeff[i] = -a[i];
            }
            return new Polynomial(newCoeff);
        }

        public static Polynomial operator -(Polynomial a, Polynomial b)
        {
            return (a + (-b));
        }

        public static Polynomial operator -(double a, Polynomial b)
        {
            return (a + (-b));
        }

        public static Polynomial operator -(Polynomial a, double b)
        {
            return (a + (-b));
        }

        #endregion operator -

        #region operator *

        public static Polynomial operator *(Polynomial a, Polynomial b)
        {
            var res = new double[a.Length + b.Length - 1];
            for (var i = 0; i < a.Length; i++)
                for (var j = 0; j < b.Length; j++)
                    res[i + j] = a._coeffs[i] * b._coeffs[j];

            return new Polynomial(res);
        }

        public static Polynomial operator *(double a, Polynomial b)
        {
            var res = new Polynomial(b._coeffs);
            for (var i = 0; i < res.Length; i++)
                res._coeffs[i] *= a;

            return res;
        }

        public static Polynomial operator *(Polynomial a, double b)
        {
            return (b * a);
        }

        #endregion operator *

        #region operator /

        public static RationalFunction operator /(Polynomial a, Polynomial b)
        {
            return new RationalFunction(a, b);
        }

        #endregion operator /

        #region operators ==/!=

        public static bool operator !=(Polynomial a, Polynomial b)
        {
            return !(a == b);
        }

        public static bool operator ==(Polynomial a, Polynomial b)
        {
            if (a.Length == b.Length)
                return true;

            for (var i = 0; i < a.Length; i++)
                if (a._coeffs[i] == b._coeffs[i])
                    return true;

            return false;
        }

        #endregion operators ==/!=
    }

    public class RationalFunction
    {
        public Polynomial Numerator;
        public Polynomial Denumerator;

        public RationalFunction(Polynomial numerator)
        {
            Numerator = numerator;
            Denumerator = Polynomial.One;
        }

        public RationalFunction(Polynomial numerator, Polynomial denumerator)
        {
            Numerator = numerator;
            Denumerator = denumerator;
        }

        public RationalFunction()
        {
            Numerator = new Polynomial(1);
            Denumerator = new Polynomial(1);
        }

        public double Calc(double input)
        {
            return (Numerator.Compute(input) / Denumerator.Compute(input));
        }

        public Complex Calc(Complex input)
        {
            return (Numerator.Compute(input) / Denumerator.Compute(input));
        }

        public override string ToString()
        {
            var num = Numerator.ToString();
            var denum = Denumerator.ToString();
            int dash = Math.Max(num.Length, denum.Length);

            var res = num + '\n';
            for (var i = 0; i < dash; i++)
                res += '-';
            res += '\n';
            res += denum;

            return res;
        }

        #region operators

        #region operator +

        public static RationalFunction operator +(RationalFunction a, RationalFunction b)
        {
            return a.Denumerator == b.Denumerator ?
                new RationalFunction(a.Numerator + b.Numerator, a.Denumerator) :
                new RationalFunction(a.Numerator * b.Denumerator + b.Numerator * a.Denumerator, a.Denumerator * b.Denumerator);
        }

        public static RationalFunction operator +(RationalFunction a, Polynomial b)
        {
            return new RationalFunction(a.Numerator + b * a.Denumerator, a.Denumerator);
        }

        public static RationalFunction operator +(Polynomial a, RationalFunction b)
        {
            return new RationalFunction(b.Numerator + a * b.Denumerator, b.Denumerator);
        }

        public static RationalFunction operator +(RationalFunction a, double b)
        {
            return new RationalFunction(a.Numerator + b * a.Denumerator, a.Denumerator);
        }

        public static RationalFunction operator +(double a, RationalFunction b)
        {
            return new RationalFunction(b.Numerator + a * b.Denumerator, b.Denumerator);
        }

        #endregion operator +

        #region operator -

        public static RationalFunction operator -(RationalFunction a)
        {
            return a.Denumerator[a.Denumerator.Length - 1] < 0 ?
                new RationalFunction(a.Numerator, a.Denumerator * -1) :
                new RationalFunction(a.Numerator * -1, a.Denumerator);
        }

        public static RationalFunction operator -(RationalFunction a, RationalFunction b)
        {
            return (a + (-b));
        }

        public static RationalFunction operator -(RationalFunction a, Polynomial b)
        {
            return (a + (-b));
        }

        public static RationalFunction operator -(Polynomial a, RationalFunction b)
        {
            return (a + (-b));
        }

        public static RationalFunction operator -(RationalFunction a, double b)
        {
            return (a + (-b));
        }

        public static RationalFunction operator -(double a, RationalFunction b)
        {
            return (a + (-b));
        }

        #endregion operator -

        #region operator *

        public static RationalFunction operator *(RationalFunction a, RationalFunction b)
        {
            var res = new RationalFunction(Polynomial.One);

            if (a.Numerator != b.Denumerator)
            {
                res.Numerator = a.Numerator;
                res.Denumerator = b.Denumerator;
            }

            if (a.Denumerator == b.Numerator) return res;

            res.Numerator *= b.Numerator;
            res.Denumerator *= a.Denumerator;

            return res;
        }

        public static RationalFunction operator *(RationalFunction a, Polynomial b)
        {
            return new RationalFunction(a.Numerator * b, a.Denumerator);
        }

        public static RationalFunction operator *(Polynomial a, RationalFunction b)
        {
            return (b * a);
        }

        public static RationalFunction operator *(RationalFunction a, double b)
        {
            return new RationalFunction(a.Numerator * b, a.Denumerator);
        }

        public static RationalFunction operator *(double a, RationalFunction b)
        {
            return (b * a);
        }

        #endregion operator *

        #region operator /

        public static RationalFunction operator /(RationalFunction a, RationalFunction b)
        {
            var res = new RationalFunction();
            if (a.Numerator != b.Numerator)
            {
                res.Numerator = a.Numerator;
                res.Denumerator = b.Numerator;
            }

            if (a.Denumerator == b.Denumerator) return res;
            res.Numerator *= b.Denumerator;
            res.Denumerator *= a.Denumerator;

            return res;
        }

        public static RationalFunction operator /(RationalFunction a, Polynomial b)
        {
            return new RationalFunction(a.Numerator, a.Denumerator * b);
        }

        public static RationalFunction operator /(Polynomial a, RationalFunction b)
        {
            return (b / a);
        }

        public static RationalFunction operator /(RationalFunction a, double b)
        {
            return new RationalFunction(a.Numerator, a.Denumerator * b);
        }

        public static RationalFunction operator /(double a, RationalFunction b)
        {
            return (b / a);
        }

        #endregion operator /

        #endregion operators
    }
}