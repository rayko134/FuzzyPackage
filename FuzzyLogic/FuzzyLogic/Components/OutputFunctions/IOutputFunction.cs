﻿namespace FuzzyLogic.Components.OutputFunctions
{
    /// <summary>
    /// A structure that converts inputs vector to an output vector.
    /// <para>Value stores the result from Compute(). </para>
    /// <para>Compute utilizes data from linguistic variables and does not have an input. </para>
    /// <para>Variables store inputs and linguistic data.</para>
    /// <para>Reset is needed for those structures that have states.</para>
    /// </summary>
    public interface IOutputFunction<out T>
    {
        T[] Value { get; }

        T[] Compute();

        int OutputLength { get; }

        void Reset();
    }
}