﻿using System;

namespace FuzzyLogic.Components.MembershipFunctions
{
    public class Gaussian : MembershipFunction
    {
        public override double Start => Scale * (Bias - Variance * MembershipBuilder.N);
        public override double End => Scale * (Bias + Variance * MembershipBuilder.N);

        public double Bias { get; }
        public double Variance { get; }

        /// <summary>
        /// A Gaussian shaped function.
        /// </summary>
        /// <param name="bias">Position of the peak</param>
        /// <param name="variance">A positive value. Higher it is, wider is the shape.</param>
        public Gaussian(double bias, double variance)
        {
            Bias = bias;
            Variance = Math.Abs(variance);
            Scale = 1;
        }

        public override double GetMembership(double x)
        {
            var sBias = Scale * Bias;
            var sVar = Variance * Scale;

            var res = Math.Exp(-0.5 * Math.Pow((x - sBias) / sVar, 2));

            return res;
        }
    }
}