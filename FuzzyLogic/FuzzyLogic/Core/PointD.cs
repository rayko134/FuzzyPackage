﻿using System;
using System.Numerics;

namespace FuzzyLogic.Core
{
    public struct PointD
    {
        public static PointD Zero = new PointD(0, 0);

        public double X;
        public double Y;
        public double Distance => Math.Sqrt(X * X + Y * Y);

        public double Arg => new Complex(X, Y).Phase;
        public bool IsNull => (X == 0 && Y == 0);

        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }

        public PointD(Complex i)
        {
            X = i.Real;
            Y = i.Imaginary;
        }

        public PointD(object x, object y)
        {
            X = Convert.ToDouble(x);
            Y = Convert.ToDouble(y);
        }

        public PointD(float x, float y) : this((double)x, (double)y)
        {
        }

        public PointD(int x, int y) : this((double)x, (double)y)
        {
        }

        public override string ToString()
        {
            return $"X: {X}; Y: {Y}.";
        }

        #region Operators

        public static PointD operator +(PointD a, PointD b)
        {
            return new PointD(a.X + b.X, a.Y + b.Y);
        }

        public static PointD operator -(PointD a, PointD b)
        {
            return new PointD(a.X - b.X, a.Y - b.Y);
        }

        public static PointD operator -(PointD a)
        {
            return new PointD(-a.X, -a.Y);
        }

        #endregion Operators
    }
}