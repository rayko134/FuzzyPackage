﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Plotter
{
    public partial class ChartForm : Form
    {
        //Data Exchange
        public List<string> Names = new List<string>(); //series names

        //Global Settings
        public double AxisScale = 1.2; //extends the limits of an axis by this number

        public double CursorInterval = 0.001;
        public double DefaultBorder = 0.01;

        //Global Trackers
        private readonly List<string> _listSelection = new List<string>();

        private int _prevScale = 1;

        //Mouse coordinates on a plot.
        private double _mouseX;

        private double _mouseY;

        public ChartForm()
        {
            InitializeComponent();

            chart1.Series.Clear();
            chart1.ChartAreas[0].CursorX.Interval = CursorInterval;
            chart1.ChartAreas[0].CursorY.Interval = CursorInterval;
            chart1.ChartAreas[0].CursorX.SetCursorPosition(0);
            chart1.ChartAreas[0].CursorY.SetCursorPosition(0);

            chart1.ChartAreas[0].CursorX.LineColor = Color.Black;
            chart1.ChartAreas[0].CursorY.LineColor = Color.Black;

            Show();
        }

        public ChartForm(string name)
        {
            InitializeComponent();

            chart1.Series.Clear();
            chart1.ChartAreas[0].CursorX.Interval = CursorInterval;
            chart1.ChartAreas[0].CursorY.Interval = CursorInterval;
            Text = name;
        }

        public void AddPoint(double x, double y, int index)
        {
            chart1.Series[index].Points.AddXY(x, y);
        }

        public void AddSeries(string name)
        {
            if (chart1.Series.Any(t => t.Name == name))
                return;

            chart1.Series.Add(name);
            chart1.Series[name].ChartType = SeriesChartType.Line;
            chart1.ChartAreas.First().AxisX.Maximum = 0.001;
            chart1.Legends.Add(name);

            listBox1.Items.Add(name);
            Names.Add(name);
        }

        public void WriteData(double x, double y, string name)
        {
            if (IsDisposed)
                return;
            if (chart1.Series.All(t => t.Name != name))
                AddSeries(name);

            chart1.Series[name].Points.AddXY(x, y);
        }

        public void WriteData(PointF pt, string name)
        {
            WriteData(pt.X, pt.Y, name);
        }

        public void WriteData(double[] x, double[] y, string name)
        {
            if (x.Length != y.Length)
            {
                MessageBox.Show("The lengths of X and Y data are different!");
                return;
            }
            for (var i = 0; i < x.Length; i++)
            {
                WriteData(x[i], y[i], name);
            }
        }

        public void WriteData(PointF[] pts, string name)
        {
            foreach (var pt in pts)
                WriteData(pt, name);
        }

        public void WriteData(params Series[] series)
        {
            foreach (var s in series)
            {
                AddSeries(s.Name);
                chart1.Series[s.Name] = s;
            }
        }

        public Series GetData(string name)
        {
            if (chart1.Series.All(t => t.Name != name))
                return new Series();

            return chart1.Series[name];
        }

        public Series[] GetData()
        {
            return chart1.Series.ToArray();
        }

        private static double SmartRound(double value, int digits)
        {
            if (value == 0)
                return value;

            double res = 0;

            int bigAmount = 0, smallAmount = 0;
            var abs = Math.Abs(value);
            double i = 1;

            while (abs > i)
            {
                i *= 10;
                bigAmount++;
            }

            if (bigAmount - digits >= 0)
            {
                res = value / Math.Pow(10, bigAmount - digits);
                res = Math.Round(res);
                res *= Math.Pow(10, bigAmount - digits);
                return res;
            }

            var diff = abs - Math.Truncate(abs);
            if (diff == 0)
                return value;

            i = 0.1;
            while (diff <= i)
            {
                i /= 10;
                smallAmount++;
            }

            if (bigAmount == 0)
                res = Math.Round(value, Math.Min(smallAmount + digits, 15));
            else
                res = Math.Round(value, digits - bigAmount);
            return res;
        }

        public void RescaleChart(int pixX, int pixY)
        {
            chart1.Width = pixX;
            chart1.Height = pixY;
        }

        public void Display()
        {
            RescaleAxes();
            Show();
        }

        public void RescaleAxes(bool selective = false)
        {
            if (listBox1.SelectedItems.Count == 0 && selective)
                return;

            double maxY, minY;
            double maxX, minX;

            if (!selective)
            {
                maxY = chart1.Series[(string)listBox1.Items[0]].Points.Max(p => p.YValues.Max());
                minY = chart1.Series[(string)listBox1.Items[0]].Points.Min(p => p.YValues.Min());
                maxX = chart1.Series[(string)listBox1.Items[0]].Points.Max(p => p.XValue);
                minX = chart1.Series[(string)listBox1.Items[0]].Points.Min(p => p.XValue);
            }
            else
            {
                maxY = chart1.Series[(string)listBox1.SelectedItems[0]].Points.Max(p => p.YValues.Max());
                minY = chart1.Series[(string)listBox1.SelectedItems[0]].Points.Min(p => p.YValues.Min());
                maxX = chart1.Series[(string)listBox1.SelectedItems[0]].Points.Max(p => p.XValue);
                minX = chart1.Series[(string)listBox1.SelectedItems[0]].Points.Min(p => p.XValue);
            }

            foreach (var s in listBox1.Items)
            {
                if (listBox1.SelectedItems.Contains(s) || !selective)
                {
                    maxY = Math.Max(maxY, chart1.Series[(string)s].Points.Max(p => p.YValues.Max()));
                    minY = Math.Min(minY, chart1.Series[(string)s].Points.Min(p => p.YValues.Min()));
                    maxX = Math.Max(maxX, chart1.Series[(string)s].Points.Max(p => p.XValue));
                    minX = Math.Min(minX, chart1.Series[(string)s].Points.Min(p => p.XValue));
                }
            }

            if (maxY == minY)
            {
                maxY += DefaultBorder;
                minY -= DefaultBorder;
            }
            if (maxX == minX)
            {
                maxX += DefaultBorder;
                minX -= DefaultBorder;
            }

            chart1.ChartAreas[0].AxisY.Maximum = SmartRound(maxY, 4);
            chart1.ChartAreas[0].AxisY.Minimum = SmartRound(minY, 4);
            chart1.ChartAreas[0].AxisX.Maximum = SmartRound(maxX, 3);
            chart1.ChartAreas[0].AxisX.Minimum = SmartRound(minX, 3);

            numeric_xMin.Value = (decimal)chart1.ChartAreas[0].AxisX.Minimum;
            numeric_xMax.Value = (decimal)chart1.ChartAreas[0].AxisX.Maximum;
            numeric_yMin.Value = (decimal)chart1.ChartAreas[0].AxisY.Minimum;
            numeric_yMax.Value = (decimal)chart1.ChartAreas[0].AxisY.Maximum;
        }

        private void SelectSeries()
        {
            foreach (var s in chart1.Series)
            {
                if (checkBox_showSelected.Checked)
                {
                    s.Enabled = listBox1.SelectedItems.Contains(s.Name);
                }
                else
                    s.Enabled = true;
            }
        }

        //
        // Events Section
        //
        private void chart1_MouseMove(object sender, MouseEventArgs e)
        {
            var posX = Math.Max(0, Math.Min(chart1.Width - 1, e.X));
            var posY = Math.Max(0, Math.Min(chart1.Height - 1, e.Y));

            chart1.ChartAreas[0].RecalculateAxesScale();
            _mouseX = chart1.ChartAreas[0].AxisX.PixelPositionToValue(posX);
            _mouseY = chart1.ChartAreas[0].AxisY.PixelPositionToValue(posY);

            double x = Math.Round(_mouseX, 3), y = Math.Round(_mouseY, 3);

            statusX.Text = "X: " + x;
            statusY.Text = "Y: " + y;
        }

        private void chart1_MouseDown(object sender, MouseEventArgs e)
        {
            chart1.ChartAreas[0].CursorX.SetCursorPosition(_mouseX);
            chart1.ChartAreas[0].CursorY.SetCursorPosition(_mouseY);

            double x = Math.Round(_mouseX, 3), y = Math.Round(_mouseY, 3);

            cursorX.Text = "Cursor X: " + x;
            cursorY.Text = "Cursor Y: " + y;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            var scale = ((TrackBar)sender).Value;
            double modifier = (float)Math.Pow(2, (double)(scale - 1) / 2);
            //chart1.Scale(new SizeF(modifier, modifier));

            var newH = panel2.Location.X - chart1.Location.X - 25;
            var newV = Height - chart1.Location.Y - 95;

            var s = new Size(Convert.ToInt32(newH * modifier), Convert.ToInt32(newV * modifier));
            chart1.Size = s;

            _prevScale = scale;
        }

        private void button_ImageSave_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "Image Files(*.png) | *.png";
            sfd.InitialDirectory = Application.StartupPath;

            if (sfd.ShowDialog() == DialogResult.OK)
                chart1.SaveImage(sfd.FileName, ChartImageFormat.Png);
        }

        private void button_ResetView_Click(object sender, EventArgs e)
        {
            trackBar1.Value = 1;

            var modifier = (float)Math.Pow(2, 1 - _prevScale);
            chart1.Scale(new SizeF(modifier, modifier));
            _prevScale = 1;

            listBox1.ClearSelected();

            checkBox_showSelected.Checked = false;
            SelectSeries();
            RescaleAxes(checkBox_showSelected.Checked);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (var i = 0; i < listBox1.Items.Count; i++)
            {
                var item = listBox1.Items[i];
                var s = (string)item;

                if (listBox1.SelectedItems.Contains(s))
                {
                    chart1.Series[s].BorderWidth = 3;
                    if (!_listSelection.Contains(s))
                        _listSelection.Add(s);
                }
                else
                {
                    chart1.Series[s].BorderWidth = 1;
                    _listSelection.Remove(s);
                }
            }

            if (listBox1.SelectedItem == null && checkBox_showSelected.Checked)
                return;

            if (!checkBox_showSelected.Checked) return;

            SelectSeries();
            RescaleAxes(checkBox_showSelected.Checked);
        }

        private void listBox1_MouseDown(object sender, MouseEventArgs e)
        {
            var index = listBox1.IndexFromPoint(e.Location);
            if (index == -1)
                listBox1.ClearSelected();
        }

        private void checkBox_showSelected_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null && checkBox_showSelected.Checked)
                return;

            SelectSeries();

            RescaleAxes(checkBox_showSelected.Checked);
        }

        private void numeric_xMin_ValueChanged(object sender, EventArgs e)
        {
            var min = (double)numeric_xMin.Value;
            if (min >= chart1.ChartAreas[0].AxisX.Maximum)
            {
                min = chart1.ChartAreas[0].AxisX.Maximum - DefaultBorder;
            }

            chart1.ChartAreas[0].AxisX.Minimum = min;
            numeric_xMin.Value = (decimal)min;
        }

        private void numeric_yMin_ValueChanged(object sender, EventArgs e)
        {
            var min = (double)numeric_yMin.Value;
            if (min >= chart1.ChartAreas[0].AxisY.Maximum)
            {
                min = chart1.ChartAreas[0].AxisY.Maximum - DefaultBorder;
            }

            chart1.ChartAreas[0].AxisY.Minimum = min;
            numeric_yMin.Value = (decimal)min;
        }

        private void numeric_xMax_ValueChanged(object sender, EventArgs e)
        {
            var max = (double)numeric_xMax.Value;
            if (max <= chart1.ChartAreas[0].AxisX.Minimum)
            {
                max = chart1.ChartAreas[0].AxisX.Minimum + DefaultBorder;
            }

            chart1.ChartAreas[0].AxisX.Maximum = max;
            numeric_xMax.Value = (decimal)max;
        }

        private void numeric_yMax_ValueChanged(object sender, EventArgs e)
        {
            var max = (double)numeric_yMax.Value;
            if (max <= chart1.ChartAreas[0].AxisY.Minimum)
            {
                max = chart1.ChartAreas[0].AxisY.Minimum + DefaultBorder;
            }

            chart1.ChartAreas[0].AxisY.Maximum = max;
            numeric_yMax.Value = (decimal)max;
        }

        private void button_getXY_Click(object sender, EventArgs e)
        {
            if (_listSelection.Count < 2)
                return;

            var nameX = _listSelection[0];
            var nameY = _listSelection[1];

            var iMax = Math.Min(chart1.Series[nameX].Points.Count, chart1.Series[nameY].Points.Count);

            var name = "XY-" + nameX + "-" + nameY;
            AddSeries(name);
            for (var i = 0; i < iMax; i++)
            {
                WriteData(chart1.Series[nameX].Points[i].YValues[0], chart1.Series[nameY].Points[i].YValues[0], name);
            }

            RescaleAxes();
        }

        private void ChartForm_SizeChanged(object sender, EventArgs e)
        {
            panel2.Location = new Point(Size.Width - panel2.Width - 16, panel2.Location.Y);

            var newH = Math.Max(1, panel2.Location.X - chart1.Location.X - 15);
            var newV = Math.Max(1, Height - chart1.Location.Y - 80);

            var modifier = (float)Math.Pow(2, (double)(_prevScale - 1) / 2);

            var s = new Size(Convert.ToInt32(newH * modifier), Convert.ToInt32(newV * modifier));
            var s1 = new Size(newH + 3, newV + 3);
            panel1.Size = s1;
            chart1.Size = s;
        }

        private void numericChartWidth_ValueChanged(object sender, EventArgs e)
        {
            chart1.Width = Convert.ToInt32(numericChartWidth.Value);
        }

        private void numericChartHeight_ValueChanged(object sender, EventArgs e)
        {
            chart1.Height = Convert.ToInt32(numericChartHeight.Value);
        }
    }
}