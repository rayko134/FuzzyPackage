﻿namespace Plotter
{
    partial class ChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusX = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusY = new System.Windows.Forms.ToolStripStatusLabel();
            this.cursorX = new System.Windows.Forms.ToolStripStatusLabel();
            this.cursorY = new System.Windows.Forms.ToolStripStatusLabel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button_ImageSave = new System.Windows.Forms.Button();
            this.button_ResetView = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.checkBox_showSelected = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numeric_xMin = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numeric_xMax = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numeric_yMin = new System.Windows.Forms.NumericUpDown();
            this.numeric_yMax = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.numericChartHeight = new System.Windows.Forms.NumericUpDown();
            this.numericChartWidth = new System.Windows.Forms.NumericUpDown();
            this.button_getXY = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_xMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_xMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_yMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_yMax)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.AxisX.IsStartedFromZero = false;
            chartArea1.AxisX2.IsStartedFromZero = false;
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.AxisY2.IsStartedFromZero = false;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 3);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(669, 421);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseDown);
            this.chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseMove);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.chart1);
            this.panel1.Location = new System.Drawing.Point(12, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 427);
            this.panel1.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusX,
            this.statusY,
            this.cursorX,
            this.cursorY});
            this.statusStrip1.Location = new System.Drawing.Point(0, 438);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(821, 24);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusX
            // 
            this.statusX.AutoSize = false;
            this.statusX.Name = "statusX";
            this.statusX.Size = new System.Drawing.Size(60, 19);
            this.statusX.Text = "X:";
            this.statusX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusY
            // 
            this.statusY.AutoSize = false;
            this.statusY.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusY.Name = "statusY";
            this.statusY.Size = new System.Drawing.Size(60, 19);
            this.statusY.Text = "Y:";
            this.statusY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cursorX
            // 
            this.cursorX.AutoSize = false;
            this.cursorX.Name = "cursorX";
            this.cursorX.Size = new System.Drawing.Size(100, 19);
            this.cursorX.Text = "Cursor X:";
            this.cursorX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cursorY
            // 
            this.cursorY.AutoSize = false;
            this.cursorY.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.cursorY.Name = "cursorY";
            this.cursorY.Size = new System.Drawing.Size(100, 19);
            this.cursorY.Text = "Cursor Y:";
            this.cursorY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(3, 3);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox1.Size = new System.Drawing.Size(120, 69);
            this.listBox1.TabIndex = 3;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDown);
            // 
            // button_ImageSave
            // 
            this.button_ImageSave.Location = new System.Drawing.Point(3, 101);
            this.button_ImageSave.Name = "button_ImageSave";
            this.button_ImageSave.Size = new System.Drawing.Size(120, 23);
            this.button_ImageSave.TabIndex = 4;
            this.button_ImageSave.Text = "Save Image";
            this.button_ImageSave.UseVisualStyleBackColor = true;
            this.button_ImageSave.Click += new System.EventHandler(this.button_ImageSave_Click);
            // 
            // button_ResetView
            // 
            this.button_ResetView.Location = new System.Drawing.Point(3, 179);
            this.button_ResetView.Name = "button_ResetView";
            this.button_ResetView.Size = new System.Drawing.Size(120, 23);
            this.button_ResetView.TabIndex = 5;
            this.button_ResetView.Text = "Reset View";
            this.button_ResetView.UseVisualStyleBackColor = true;
            this.button_ResetView.Click += new System.EventHandler(this.button_ResetView_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 10;
            this.trackBar1.Location = new System.Drawing.Point(3, 249);
            this.trackBar1.Maximum = 3;
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(120, 45);
            this.trackBar1.TabIndex = 6;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar1.Value = 1;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // checkBox_showSelected
            // 
            this.checkBox_showSelected.AutoSize = true;
            this.checkBox_showSelected.Location = new System.Drawing.Point(5, 78);
            this.checkBox_showSelected.Name = "checkBox_showSelected";
            this.checkBox_showSelected.Size = new System.Drawing.Size(122, 17);
            this.checkBox_showSelected.TabIndex = 7;
            this.checkBox_showSelected.Text = "Show Only Selected";
            this.checkBox_showSelected.UseVisualStyleBackColor = true;
            this.checkBox_showSelected.CheckedChanged += new System.EventHandler(this.checkBox_showSelected_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Zoom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "X Limits";
            // 
            // numeric_xMin
            // 
            this.numeric_xMin.DecimalPlaces = 4;
            this.numeric_xMin.Location = new System.Drawing.Point(7, 313);
            this.numeric_xMin.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.numeric_xMin.Minimum = new decimal(new int[] {
            276447232,
            23283,
            0,
            -2147483648});
            this.numeric_xMin.Name = "numeric_xMin";
            this.numeric_xMin.Size = new System.Drawing.Size(86, 20);
            this.numeric_xMin.TabIndex = 10;
            this.numeric_xMin.ValueChanged += new System.EventHandler(this.numeric_xMin_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(99, 315);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Min";
            // 
            // numeric_xMax
            // 
            this.numeric_xMax.DecimalPlaces = 4;
            this.numeric_xMax.Location = new System.Drawing.Point(7, 339);
            this.numeric_xMax.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.numeric_xMax.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.numeric_xMax.Name = "numeric_xMax";
            this.numeric_xMax.Size = new System.Drawing.Size(86, 20);
            this.numeric_xMax.TabIndex = 12;
            this.numeric_xMax.ValueChanged += new System.EventHandler(this.numeric_xMax_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(99, 341);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Max";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Y Limits";
            // 
            // numeric_yMin
            // 
            this.numeric_yMin.DecimalPlaces = 4;
            this.numeric_yMin.Location = new System.Drawing.Point(7, 378);
            this.numeric_yMin.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.numeric_yMin.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.numeric_yMin.Name = "numeric_yMin";
            this.numeric_yMin.Size = new System.Drawing.Size(86, 20);
            this.numeric_yMin.TabIndex = 15;
            this.numeric_yMin.ValueChanged += new System.EventHandler(this.numeric_yMin_ValueChanged);
            // 
            // numeric_yMax
            // 
            this.numeric_yMax.DecimalPlaces = 4;
            this.numeric_yMax.Location = new System.Drawing.Point(7, 404);
            this.numeric_yMax.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.numeric_yMax.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.numeric_yMax.Name = "numeric_yMax";
            this.numeric_yMax.Size = new System.Drawing.Size(86, 20);
            this.numeric_yMax.TabIndex = 16;
            this.numeric_yMax.ValueChanged += new System.EventHandler(this.numeric_yMax_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(99, 406);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Max";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(99, 380);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Min";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.numericChartHeight);
            this.panel2.Controls.Add(this.numericChartWidth);
            this.panel2.Controls.Add(this.button_getXY);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.listBox1);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.button_ImageSave);
            this.panel2.Controls.Add(this.numeric_yMax);
            this.panel2.Controls.Add(this.button_ResetView);
            this.panel2.Controls.Add(this.numeric_yMin);
            this.panel2.Controls.Add(this.trackBar1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.checkBox_showSelected);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.numeric_xMax);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.numeric_xMin);
            this.panel2.Location = new System.Drawing.Point(694, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 427);
            this.panel2.TabIndex = 1;
            // 
            // numericChartHeight
            // 
            this.numericChartHeight.Location = new System.Drawing.Point(71, 130);
            this.numericChartHeight.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericChartHeight.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericChartHeight.Name = "numericChartHeight";
            this.numericChartHeight.Size = new System.Drawing.Size(52, 20);
            this.numericChartHeight.TabIndex = 21;
            this.numericChartHeight.Value = new decimal(new int[] {
            421,
            0,
            0,
            0});
            this.numericChartHeight.ValueChanged += new System.EventHandler(this.numericChartHeight_ValueChanged);
            // 
            // numericChartWidth
            // 
            this.numericChartWidth.Location = new System.Drawing.Point(3, 130);
            this.numericChartWidth.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericChartWidth.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericChartWidth.Name = "numericChartWidth";
            this.numericChartWidth.Size = new System.Drawing.Size(62, 20);
            this.numericChartWidth.TabIndex = 20;
            this.numericChartWidth.Value = new decimal(new int[] {
            669,
            0,
            0,
            0});
            this.numericChartWidth.ValueChanged += new System.EventHandler(this.numericChartWidth_ValueChanged);
            // 
            // button_getXY
            // 
            this.button_getXY.Location = new System.Drawing.Point(3, 208);
            this.button_getXY.Name = "button_getXY";
            this.button_getXY.Size = new System.Drawing.Size(120, 23);
            this.button_getXY.TabIndex = 19;
            this.button_getXY.Text = "Get XY from Selected";
            this.button_getXY.UseVisualStyleBackColor = true;
            this.button_getXY.Click += new System.EventHandler(this.button_getXY_Click);
            // 
            // ChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 462);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Name = "ChartForm";
            this.Text = "Chart";
            this.SizeChanged += new System.EventHandler(this.ChartForm_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_xMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_xMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_yMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_yMax)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartWidth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusX;
        private System.Windows.Forms.ToolStripStatusLabel statusY;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button_ImageSave;
        private System.Windows.Forms.Button button_ResetView;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.ToolStripStatusLabel cursorX;
        private System.Windows.Forms.ToolStripStatusLabel cursorY;
        private System.Windows.Forms.CheckBox checkBox_showSelected;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numeric_xMin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numeric_xMax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numeric_yMin;
        private System.Windows.Forms.NumericUpDown numeric_yMax;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_getXY;
        private System.Windows.Forms.NumericUpDown numericChartHeight;
        private System.Windows.Forms.NumericUpDown numericChartWidth;
    }
}