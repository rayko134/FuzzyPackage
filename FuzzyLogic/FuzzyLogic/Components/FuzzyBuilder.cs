﻿using FuzzyLogic.Components.Defuzzifiers;
using FuzzyLogic.Components.Expressions;
using FuzzyLogic.Components.MembershipFunctions;
using FuzzyLogic.Components.OutputFunctions;
using FuzzyLogic.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuzzyLogic.Components
{
    /// <summary>
    /// A partial class that provides quick solutions for building FuzzyLogic components.
    /// </summary>
    public partial class FuzzyBuilder
    {
        private double _fuzziness;

        public double Fuzziness
        {
            get => _fuzziness;
            set => _fuzziness = Math.Min(1, Math.Max(0, value));
        }

        public bool Open;
        public bool Continuous;

        /// <param name="fuzziness">Value from 0 to 1. 1 means membership functions are very fuzzy (cover neighbour functions by 50%)</param>
        /// <param name="open">If true, values outside range belong to the first/last membership function.</param>
        /// <param name="continuous">If true, uses gaussians, else uses trapezoids.</param>
        public FuzzyBuilder(double fuzziness = 0.5, bool open = true, bool continuous = false)
        {
            Fuzziness = fuzziness;
            Open = open;
            Continuous = continuous;
        }

        /// <summary>
        /// Make a fuzzy module that mimics a given piecewise linear law.
        /// </summary>
        /// <param name="breakers">Points which shape the law.</param>
        /// <param name="saturated">If true, makes the output saturated</param>
        /// <returns></returns>
        public FuzzyLogic<double[]> MakePiecewiseLinear(PointD[] breakers, bool saturated = true)
        {
            var intervals = breakers.Select(t => t.X).ToArray();
            var variable = MakeLinguisticVariable("Transform", intervals);

            var rules = MakeRulesFromVariable(variable);

            var amount = breakers.Length + 1; //-1 for intervals + 2 for sides
            var last = amount - 1;
            var outputs = new PolynomialOutput[amount];

            for (var i = 1; i < last; i++)
            {
                var j = i - 1; // 0 to breakers.Length - 1
                var poly = new Polynomial(breakers[j + 1], breakers[j]);
                outputs[i] = new PolynomialOutput(variable, poly);
            }

            if (saturated)
            {
                outputs[0] = new PolynomialOutput(variable, breakers[0].Y);
                outputs[last] = new PolynomialOutput(variable, breakers.Last().Y);
            }
            else
            {
                outputs[0] = outputs[1];
                outputs[last] = outputs[last - 1];
            }

            var fuzzyfier = new Fuzzifier(variable);
            var rulebase = new FuzzyInference(rules);
            var defuzzifier = new SugenoDefuzzifier(rulebase, outputs);

            return new FuzzyLogic<double[]>(fuzzyfier, rulebase, defuzzifier);
        }

        /// <summary>
        /// Makes rules via Carthesian product of the variables' linguistic values.
        /// </summary>
        /// <param name="variables"></param>
        /// <returns></returns>
        public List<IExpression> MakeRulesByCarthesianProduct(params LinguisticVariable[] variables)
        {
            var list = new List<CircularList<ExpressionIs>>();
            var watchers = new List<bool>();

            //Populating list with expressions 'varX IS valueY'
            foreach (var v in variables)
            {
                var varlist = new List<LinguisticValue>();
                varlist.AddRange(v.FuzzyRange);
                list.Add(new CircularList<ExpressionIs>());

                foreach (var lv in varlist)
                    list.Last().Add(new ExpressionIs(v, lv));

                watchers.Add(false);
            }

            var res = new List<IExpression>();
            while (!watchers.Last())
            {
                //Computing total AND expression
                Expression exp = list[0].Current;
                for (var i = 1; i < list.Count; i++)
                {
                    exp = exp.And(list[i].Current);
                }

                res.Add(exp);

                watchers[0] = list[0].Next();
                //After rotation of the first ring we check if others should be rotated as well
                for (var i = 0; i < list.Count - 1; i++)
                {
                    if (watchers[i])
                    {
                        watchers[i + 1] = list[i + 1].Next();
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return res;
        }

        public IExpression[] MakeRulesFromVariable(LinguisticVariable variable)
        {
            var res = new IExpression[variable.FuzzyRange.Length];

            for (var i = 0; i < res.Length; i++)
                res[i] = new ExpressionIs(variable, i);

            return res;
        }

        /// <summary>
        /// Creates a LinguisticVariable based on it's range and the amount of membership functions.
        /// Created membership functions cover each other depending on fuzziness parameter.
        /// </summary>
        /// <param name="name">Name of a LinguisticVariable</param>
        /// <param name="start">Left end of the range.</param>
        /// <param name="end">Right end of the range.</param>
        /// <param name="membershipsAmount">Amount of membership functions to be distributed.</param>
        /// <returns></returns>
        public LinguisticVariable MakeLinguisticVariable(string name, double start, double end, int membershipsAmount)
        {
            //Fuzzy range midpoints
            var midpoints = new double[membershipsAmount];
            var last = midpoints.Length - 1;

            var sidesAmount = Open ? 2 : 0;

            var step = (end - start) / (midpoints.Length - sidesAmount);

            //Width of membership functions
            var halfwidth = step / 2 + step * Fuzziness / 2; //Step if fuzziness = 1, Step/2 if fuzziness = 0;
            var halfwidths = new double[membershipsAmount];

            //Intervals
            var breakersAmount = membershipsAmount - sidesAmount + 1;
            var breakers = new double[breakersAmount];
            for (var i = 0; i < breakers.Length; i++)
                breakers[i] = start + i * step;

            //Arrays population
            if (Open)
            {
                midpoints[0] = start;
                halfwidths[0] = halfwidth - step / 2;
                midpoints[last] = end;
                halfwidths[last] = halfwidth - step / 2;
            }
            else
            {
                midpoints[0] = start + step / 2;
                halfwidths[0] = halfwidth;
                midpoints[last] = end - step / 2;
                halfwidths[last] = halfwidth;
            }

            for (var i = 1; i < last; i++)
            {
                //shift by -step/2 as first function is only step/2 wide.
                var shift = (Open) ? step / 2 : 0;
                midpoints[i] = midpoints[0] - shift + i * step;
                halfwidths[i] = halfwidth;
            }

            var fuzzyrange = MakeFuzzyRange(midpoints, halfwidths, breakers);

            return new LinguisticVariable(name, fuzzyrange);
        }

        /// <summary>
        /// Creates a LinguisticVariable based on the given intervals' data.
        /// </summary>
        /// <param name="name">Name of a LinguisticVariable</param>
        /// <param name="breakers">Points that separate intervals</param>
        /// <returns></returns>
        public LinguisticVariable MakeLinguisticVariable(string name, double[] breakers)
        {
            if (breakers.Length == 1) throw new Exception("Not enough points to make an interval for making a linguistic variable.");

            var sidesAmount = Open ? 2 : 0;

            var amount = breakers.Length - 1 + sidesAmount; //-1 for intervals + 2 for sides
            var last = amount - 1;
            var breakersLast = breakers.Length - 1;

            //Midpoints computation
            var midpoints = new double[amount];

            if (Open)
            {
                midpoints[0] = breakers[0];
                midpoints[last] = breakers[breakersLast];
            }
            else
            {
                //same as midpoints in the middle
                midpoints[0] = (breakers[1] + breakers[0]) / 2.0;
                midpoints[last] = (breakers[breakersLast] + breakers[breakersLast - 1]) / 2.0;
            }

            for (var i = 1; i < last; i++)
            {
                //Linear transient function depending on fuzziness
                var j = i - 1; //in open case
                j += 1 - sidesAmount / 2; //in closed case the change is non-zero
                //Middle of every interval
                midpoints[i] = (breakers[j + 1] + breakers[j]) / 2.0;
            }

            //Halfwidths computation
            var halfwidths = new double[amount];

            if (Open)
            {
                halfwidths[0] = (midpoints[1] - midpoints[0]) * Fuzziness;
                halfwidths[last] = (midpoints[last] - midpoints[last - 1]) * Fuzziness;
            }
            else
            {
                //same as halfwidths in the middle
                halfwidths[0] = 0.5 * (midpoints[1] - midpoints[0]) * (1 + Fuzziness);
                halfwidths[last] = 0.5 * (midpoints[last] - midpoints[last - 1]) * (1 + Fuzziness);
            }

            for (var i = 1; i < last; i++)
            {
                //Linear transient function depending on fuzziness
                var j = i - 1; //in open case
                j += 1 - sidesAmount / 2; //in closed case the change is non-zero

                // Automated means of membership functions' creation do not support asymmetric functions
                // Therefore, in case of asymmetry, maximum is taken and then is properly cut via limiters.
                var left = (midpoints[i] - midpoints[i - 1]);
                var right = (midpoints[i + 1] - midpoints[i]);

                halfwidths[i] = 0.5 * Math.Max(left, right) * (1 + Fuzziness);
            }

            var fuzzyrange = MakeFuzzyRange(midpoints, halfwidths, breakers);

            return new LinguisticVariable(name, fuzzyrange);
        }

        /// <summary>
        /// Creates membership functions using a prepared data.
        /// </summary>
        /// <param name="midpoints">Middle points of membership functions.</param>
        /// <param name="halfwidths">Half of the width of a base of corresponding membership function.</param>
        /// <param name="breakers">Intervals' separators for limiters curation.</param>
        /// <returns></returns>
        public IMembershipFunction[] MakeFuzzyRange(double[] midpoints, double[] halfwidths, double[] breakers)
        {
            var sidesAmount = Open ? 2 : 0;

            //Types setup for reduced branching
            var types = GetMembershipTypes();

            //Membership Setup
            MembershipBuilder builder = new MembershipBuilder(Fuzziness);
            var fuzzyrange = new IMembershipFunction[midpoints.Length];
            var last = midpoints.Length - 1;
            var breakersLast = breakers.Length - 1;
            var start = breakers[0];
            var end = breakers[breakersLast];

            //Specifying min/max limiters is a safety measure to avoid excessive overlapping
            fuzzyrange[0] = builder.Build(midpoints[0], halfwidths[0], types[0], start, midpoints[1]);
            fuzzyrange[last] = builder.Build(midpoints[last], halfwidths[last], types[2], midpoints[last - 1], end);
            for (var i = 1; i < last; i++)
            {
                //Linear transient function depending on fuzziness
                var j = i - 1; //in open case
                j += 1 - sidesAmount / 2; //in closed case the change is non-zero
                var min = midpoints[i - 1] * Fuzziness + breakers[j] * (1 - Fuzziness);
                var max = midpoints[i + 1] * Fuzziness + breakers[j + 1] * (1 - Fuzziness);

                fuzzyrange[i] = builder.Build(midpoints[i], halfwidths[i], types[1],
                                              min, max);
            }

            return fuzzyrange;
        }

        /// <summary>
        /// Gets MembershipFunctionType types to build a fuzzy set. Uses 'Open' and 'Continuous' booleans of the FuzzyBuilder.
        /// </summary>
        /// <returns>An array[3]. 0 and 2 are left and right edge types, 1 is the middle type.</returns>
        private MembershipFunctionType[] GetMembershipTypes()
        {
            var types = new MembershipFunctionType[3];
            if (Continuous)
            {
                types[0] = Open ? MembershipFunctionType.SigmoidLeft
                    : MembershipFunctionType.Gaussian;
                types[1] = MembershipFunctionType.Gaussian;
                types[2] = Open ? MembershipFunctionType.SigmoidRight
                    : MembershipFunctionType.Gaussian;
            }
            else
            {
                types[0] = Open ? MembershipFunctionType.TrapezoidLeftSide
                    : MembershipFunctionType.Trapezoid;
                types[1] = MembershipFunctionType.Trapezoid;
                types[2] = Open ? MembershipFunctionType.TrapezoidRightSide
                    : MembershipFunctionType.Trapezoid;
            }

            return types;
        }
    }
}