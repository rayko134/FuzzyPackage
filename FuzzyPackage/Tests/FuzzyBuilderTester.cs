﻿using FuzzyLogic;
using FuzzyLogic.Components;
using FuzzyLogic.Components.Defuzzifiers;
using FuzzyLogic.Components.MembershipFunctions;
using FuzzyLogic.Components.OutputFunctions;
using FuzzyLogic.Core;
using Plotter;
using System;
using System.Collections.Generic;

namespace FuzzyPackage
{
    public partial class Tester
    {
        public void MamdaniTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var builder = new FuzzyBuilder(Fuzziness, Open, Continous);

            var var1 = builder.MakeLinguisticVariable("Variable 1", -10, 10, 3);
            DisplayFuzzyRange(var1);

            var rules = builder.MakeRulesFromVariable(var1);

            //Manual output functions creation
            var mfBuilder = new MembershipBuilder(Fuzziness);
            var type = (Continous) ? MembershipFunctionType.Gaussian : MembershipFunctionType.Trapezoid;
            var out1 = new FuzzyOutput(mfBuilder.Build(50, 20, type, 30, 100));
            var out2 = new FuzzyOutput(mfBuilder.Build(80, 30, type, 50, 125));
            var out3 = new FuzzyOutput(mfBuilder.Build(100, 15, type, 80, 150));
            IOutputFunction<IMembershipFunction>[] outs = { out1, out2, out3 };

            var fuzzifier = new Fuzzifier(var1);
            var rulebase = new FuzzyInference(rules);
            var defuzzifier = new MamdaniDefuzzifier(rulebase, outs);

            var fuzzyLogic = new FuzzyLogic<double[]>(fuzzifier, rulebase, defuzzifier);

            DisplayTimeSeries(fuzzyLogic, t => 2 * t - 11, 11);
        }

        public void MakeLinguisticVariableTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var builder = new FuzzyBuilder(Fuzziness, Open, Continous);

            var start = -10;
            var end = 10;
            var amount = 5;

            Logger.WriteLine($"Starting data: start = {start}, end = {end}, amount = {amount}");

            Logger.WriteLine("Creation via range and amount of membership functions...");
            var var1 = builder.MakeLinguisticVariable("Variable1", start, end, amount);
            Logger.WriteLine($"Created {var1.Name}.");
            DisplayFuzzyRange(var1);

            //Construct similar fuzzy set using other overload.
            var sidesAmount = (Open) ? 2 : 0;
            var pts = new double[amount - sidesAmount + 1];
            pts[0] = start;
            pts[pts.Length - 1] = end;

            for (var i = 1; i < pts.Length - 1; i++)
                pts[i] = start + 1.0 * i * (end - start) / (amount - sidesAmount);

            Logger.WriteLine("Creation via breakers: attempting to copy Variable 1...");
            var var2 = builder.MakeLinguisticVariable("Variable 2", pts);
            Logger.WriteLine($"Created {var2.Name}.");

            DisplayFuzzyRange(var2);

            //Construct asymmetric fuzzy set
            var breakers = new double[7] { -5, -4, 0, 10, 13, 13.1, 20 };
            Logger.WriteLine("Creation via breakers: a heavily asymmetric fuzzy set");
            var var3 = builder.MakeLinguisticVariable("Variable 3", breakers);
            Logger.WriteLine($"Created {var3.Name}.");
            DisplayFuzzyRange(var3);
        }

        public void MakePiecewiseLinearTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var builder = new FuzzyBuilder(Fuzziness, Open, Continous);

            var rawPts = new List<PointD>();
            rawPts.Add(new PointD(1, 10));
            rawPts.Add(new PointD(2, -5));
            rawPts.Add(new PointD(5, 7));

            //Create a full set of points
            var pts = GetSymmetricPoints(rawPts.ToArray());

            Logger.WriteLine("Points to produce piecewise linear function:");
            foreach (var pt in pts)
            {
                Logger.WriteLine(pt.ToString());
            }

            var fuzzy = builder.MakePiecewiseLinear(pts);
            fuzzy.Evaluate(2);

            DisplayFuzzyRange(fuzzy.Fuzzifier.LinguisticVariables[0]);
            DisplayFuzzyIo(fuzzy, -7, 7);
            DisplayTimeSeries(fuzzy, t => 10 * Math.Sin(t));
        }

        public void BuilderCarthesianProductTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var builder = new FuzzyBuilder(Fuzziness, Open, Continous);

            const int aStart = 0, aEnd = 10, aAmount = 5;
            const int bStart = 1, bEnd = 9, bAmount = 3;

            var a = builder.MakeLinguisticVariable("A", aStart, aEnd, aAmount);
            var b = builder.MakeLinguisticVariable("B", bStart, bEnd, bAmount);

            var rules = builder.MakeRulesByCarthesianProduct(a, b);

            Logger.WriteLine($"Built a 'A x B' product: {rules.Count} rules produced as follows:");

            DisplayFuzzyRange(a);
            DisplayFuzzyRange(b);

            var form1 = new ChartForm("Memberships by rules");
            var start = Math.Min(aStart, bStart);
            var end = Math.Max(aEnd, bEnd);
            var step = 1.0 * (end - start) / Points;
            var i = 1;

            foreach (var rule in rules)
            {
                Logger.WriteLine(rule.Name);
                for (var j = 0; j < Points; j++)
                {
                    var input = start + step * j;
                    a.Input = input;
                    b.Input = input;
                    var output = rule.Value;
                    form1.WriteData(input, output, "Rule " + i);
                }

                ++i;
            }

            form1.Display();
        }
    }
}