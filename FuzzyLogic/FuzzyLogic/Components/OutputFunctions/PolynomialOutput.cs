﻿using FuzzyLogic.Core;

namespace FuzzyLogic.Components.OutputFunctions
{
    /// <summary>
    /// A polynomial function of a single variable.
    /// </summary>
    public class PolynomialOutput : IOutputFunction<double>
    {
        public double[] Value { get; }
        public int OutputLength => Value.Length;

        public LinguisticVariable Variable { get; }

        private Polynomial _poly;

        public PolynomialOutput(LinguisticVariable variable, Polynomial poly)
        {
            Variable = variable;
            _poly = poly;
            Value = new double[1];
        }

        public PolynomialOutput(LinguisticVariable variable, params double[] coefficients) :
            this(variable, new Polynomial(coefficients))
        { }

        public double[] Compute()
        {
            Value[0] = _poly.Compute(Variable.Input);
            return Value;
        }

        public void Reset()
        {
            Value[0] = 0;
        }
    }
}