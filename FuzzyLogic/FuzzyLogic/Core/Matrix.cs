﻿using System;
using System.Globalization;
using System.Linq;

namespace FuzzyLogic.Core
{
    /// <summary>
    /// A vessel for 2D-array data to reduce code repetition and ease the implementation of Matrices and Vectors.
    /// </summary>
    internal struct DataHolder
    {
        public double this[int i, int j]
        {
            get => Values[i, j];
            set => Values[i, j] = value;
        }

        public int Rows => Values.GetLength(0);
        public int Columns => Values.GetLength(1);

        public double[,] Values;

        public DataHolder(int rows, int columns)
        {
            Values = new double[rows, columns];
        }

        public DataHolder(double[,] values)
        {
            Values = values;
        }

        public void Fill(double value)
        {
            for (var i = 0; i < Values.GetLength(0); i++)
                for (var j = 0; j < Values.GetLength(1); j++)
                    Values[i, j] = value;
        }

        public override string ToString()
        {
            var res = "";

            for (var i = 0; i < Rows; i++)
            {
                res += $"Row {i}:";
                for (var j = 0; j < Columns; j++)
                {
                    res += $"\t{ this[i, j]}";
                }

                res += '\n';
            }

            return res;
        }

        //General matrix rules of summing and multiplication.

        #region Operators

        public static DataHolder operator *(DataHolder a, DataHolder b)
        {
            if (a.Columns != b.Rows)
                throw new Exception("Structures are incompatible: multiplication of " +
                                    $"{a.Rows}x{a.Columns} by {b.Rows}x{b.Columns} is not allowed.");

            var res = new DataHolder(a.Rows, b.Columns);
            var lim = a.Columns; //== b.Rows

            for (var i = 0; i < res.Rows; i++)
                for (var j = 0; j < res.Columns; j++)
                    for (var k = 0; k < lim; k++)
                    {
                        res.Values[i, j] += a.Values[i, k] * b.Values[k, j];
                    }

            return res;
        }

        public static DataHolder operator +(DataHolder a, DataHolder b)
        {
            if (a.Columns != b.Columns || a.Rows != b.Rows)
                throw new Exception("Structures are incompatible: combining " +
                                    $"{a.Rows}x{a.Columns} with {b.Rows}x{b.Columns} is not allowed.");

            var res = new DataHolder(a.Rows, a.Columns);

            for (var i = 0; i < a.Rows; i++)
                for (var j = 0; j < a.Columns; j++)
                    res.Values[i, j] = a.Values[i, j] + b.Values[i, j];

            return res;
        }

        public static DataHolder operator -(DataHolder a)
        {
            var res = new DataHolder(a.Rows, a.Columns);

            for (var i = 0; i < a.Rows; i++)
                for (var j = 0; j < a.Columns; j++)
                    res.Values[i, j] = -a.Values[i, j];
            return res;
        }

        public static DataHolder operator -(DataHolder a, DataHolder b)
        {
            return a + (-b);
        }

        public static DataHolder operator *(DataHolder a, double b)
        {
            var res = new DataHolder(a.Rows, a.Columns);

            for (var i = 0; i < a.Rows; i++)
                for (var j = 0; j < a.Columns; j++)
                    res.Values[i, j] = a.Values[i, j] * b;
            return res;
        }

        public static DataHolder operator *(double a, DataHolder b)
        {
            return b * a;
        }

        #endregion Operators
    }

    public struct Matrix
    {
        public int Rows => Data.Rows;
        public int Columns => Data.Columns;
        public double[,] Values => Data.Values;

        internal DataHolder Data;

        public double this[int i, int j]
        {
            get => Data[i, j];
            set => Data[i, j] = value;
        }

        internal Matrix(DataHolder data)
        {
            Data = data;
        }

        public Matrix(int rows, int columns, double value = 0)
        {
            Data = new DataHolder(rows, columns);
            Data.Fill(value);
        }

        public Matrix(double[,] matrix)
        {
            Data = new DataHolder(matrix);
        }

        /// <summary>
        /// Format: ',' separates columns, ';' separates rows, '.' is a decimal point for real numbers.
        /// Spaces are allowed.
        /// </summary>
        /// <param name="matrix">A string that represents a matrix in a specific format.</param>
        public Matrix(string matrix)
        {
            var raws = matrix.Replace(',', ' ').Replace("  ", " ");
            raws = raws.Replace("; ", ";");
            var rows = raws.Split(';');
            var rowsAmount = rows.Length;
            if (rows.Last() == "")
                --rowsAmount;

            var values = new string[rowsAmount][];
            for (var i = 0; i < rowsAmount; i++)
            {
                values[i] = rows[i].Split(' ');
            }

            var columnsAmount = values[0].Length;
            Data = new DataHolder(rowsAmount, columnsAmount);

            for (var i = 0; i < Data.Rows; i++)
                for (var j = 0; j < Data.Columns; j++)
                {
                    var res = Double.Parse(values[i][j], CultureInfo.InvariantCulture);
                    Data[i, j] = res;
                }
        }

        public void Fill(double value) => Data.Fill(value);

        public Vector SelectColumn(int index)
        {
            var res = new Vector(Rows);
            for (var i = 0; i < Rows; i++)
                res[i] = Data[i, index];
            return res;
        }

        public override string ToString()
        {
            return Data.ToString();
        }

        public static Matrix Unitary(int size)
        {
            var res = new double[size, size];

            for (var i = 0; i < size; i++)
                res[i, i] = 1;

            return new Matrix(res);
        }

        #region operators

        public static Matrix operator *(Matrix a, Matrix b) => new Matrix(a.Data * b.Data);

        public static Matrix operator +(Matrix a, Matrix b) => new Matrix(a.Data + b.Data);

        public static Matrix operator -(Matrix a) => new Matrix(-a.Data);

        public static Matrix operator -(Matrix a, Matrix b) => new Matrix(a.Data + (-b.Data));

        public static Matrix operator *(Matrix a, double b) => new Matrix(a.Data * b);

        public static Matrix operator *(double a, Matrix b) => b * a;

        public static Matrix operator *(Vector a, Matrix b) => new Matrix(a.Data * b.Data);

        #endregion operators
    }

    public struct Vector
    {
        public double[,] Values => Data.Values;
        public int Length => Data.Rows;

        internal DataHolder Data;

        public double this[int i]
        {
            get => Data[i, 0];
            set => Data[i, 0] = value;
        }

        public Vector(int length)
        {
            Data = new DataHolder(length, 1);
        }

        public Vector(params double[] values) : this(values.Length)
        {
            for (var i = 0; i < values.Length; i++)
                this[i] = values[i];
        }

        internal Vector(DataHolder data)
        {
            //Is never fed with a wrong data by the original algorithm, no checks are made
            Data = data;
        }

        public void Fill(double value) => Data.Fill(value);

        public Matrix Transpose()
        {
            var res = new Matrix(1, Length);
            for (var i = 0; i < res.Columns; i++)
                res[0, i] = this[i];
            return res;
        }

        public override string ToString()
        {
            return Data.ToString();
        }

        #region operators

        public static Vector operator +(Vector a, Vector b) => new Vector(a.Data + b.Data);

        public static Vector operator -(Vector a) => new Vector(-a.Data);

        public static Vector operator -(Vector a, Vector b) => new Vector(a.Data + (-b.Data));

        public static Vector operator *(Vector a, double b) => new Vector(a.Data * b);

        public static Vector operator *(double a, Vector b) => b * a;

        public static Vector operator *(Matrix a, Vector b) => new Vector(a.Data * b.Data);

        #endregion operators
    }
}