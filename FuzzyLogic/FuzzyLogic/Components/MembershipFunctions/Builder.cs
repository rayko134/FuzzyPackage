﻿using System;
using System.Collections.Generic;

namespace FuzzyLogic.Components.MembershipFunctions
{
    public enum MembershipFunctionType
    {
        Trapezoid,
        TrapezoidLeftSide,
        TrapezoidRightSide,
        SigmoidLeft,
        SigmoidRight,
        Gaussian,
    }

    public class MembershipBuilder
    {
        //N - 'variance steps' from the middle to the start/end of a continuous membership function.
        public const int N = 3;

        private readonly double _fz; // 0 means sharpest membership functions
        private readonly Dictionary<MembershipFunctionType, Builder> _buildTable;

        private delegate IMembershipFunction Builder(double midpoint, double halvedWidth,
            double min, double max);

        /// <param name="fuzzyness">How sharp membership functions will be. 0 is sharp, 1 - very smooth.</param>
        public MembershipBuilder(double fuzzyness)
        {
            _fz = fuzzyness;
            _buildTable = InitTable();

            if (!IsSafeToUse())
                throw new Exception("MembershipFunctionType contains a type that will cause this builder to "
                                    + "throw an exception.");
        }

        /// <summary>
        /// Builds a membership function.
        /// </summary>
        /// <param name="midpoint">Middle of the membership function.</param>
        /// <param name="halvedWidth">Half of function's width.</param>
        /// <param name="type">A type from MembershipFunctionType.</param>
        /// <param name="min">A minimal value the function can start from.</param>
        /// <param name="max">A maximal value the function can end in.</param>
        /// <returns></returns>
        public IMembershipFunction Build(double midpoint, double halvedWidth, MembershipFunctionType type,
                                         double min, double max)
        {
            return _buildTable[type](midpoint, halvedWidth, min, max);
        }

        /// <summary>
        /// Checks if MembershipFunctionType is wholly contained in a building dictionary.
        /// </summary>
        /// <returns></returns>
        public bool IsSafeToUse()
        {
            foreach (MembershipFunctionType type in Enum.GetValues(typeof(MembershipFunctionType)))
            {
                if (!_buildTable.ContainsKey(type))
                    return false;
            }

            return true;
        }

        private Dictionary<MembershipFunctionType, Builder> InitTable()
        {
            var res = new Dictionary<MembershipFunctionType, Builder>();
            res.Add(MembershipFunctionType.Trapezoid, MakeTrapezoid);
            res.Add(MembershipFunctionType.TrapezoidLeftSide, MakeTrapezoidLeftSide);
            res.Add(MembershipFunctionType.TrapezoidRightSide, MakeTrapezoidRightSide);
            res.Add(MembershipFunctionType.SigmoidLeft, MakeSigmoidLeft);
            res.Add(MembershipFunctionType.SigmoidRight, MakeSigmoidRight);
            res.Add(MembershipFunctionType.Gaussian, MakeGaussian);

            return res;
        }

        #region Building functions

        private IMembershipFunction MakeTrapezoid(double midpoint, double halvedWidth,
                                                  double min, double max)
        {
            var start = Math.Max(min, midpoint - halvedWidth);
            var end = Math.Min(max, midpoint + halvedWidth);
            var left = start + (midpoint - start) * _fz;
            var right = end - (end - midpoint) * _fz;

            return new Trapezoid(start, left, right, end);
        }

        private IMembershipFunction MakeTrapezoidLeftSide(double midpoint, double halvedWidth,
                                                          double min, double max)
        {
            var start = midpoint;
            var end = Math.Min(max, midpoint + halvedWidth);
            return new Trapezoid(start, end, Trapezoid.Side.Left);
        }

        private IMembershipFunction MakeTrapezoidRightSide(double midpoint, double halvedWidth,
                                                           double min, double max)
        {
            var start = Math.Max(min, midpoint - halvedWidth);
            var end = midpoint;
            return new Trapezoid(start, end, Trapezoid.Side.Right);
        }

        private IMembershipFunction MakeSigmoidLeft(double midpoint, double halvedWidth,
                                                    double min, double max)
        {
            var bias = midpoint;
            var inversedCurvature = (max - bias) / (N * _fz);
            var displayWidth = N * halvedWidth * (1 + _fz);

            return new Sigmoid(bias, inversedCurvature, Sigmoid.Side.Left, displayWidth);
        }

        private IMembershipFunction MakeSigmoidRight(double midpoint, double halvedWidth,
                                                     double min, double max)
        {
            var bias = midpoint;
            var inversedSteep = (bias - min) / (N * _fz);
            var displayWidth = N * halvedWidth * (1 + _fz);

            return new Sigmoid(bias, inversedSteep, Sigmoid.Side.Right, displayWidth);
        }

        private IMembershipFunction MakeGaussian(double midpoint, double halvedWidth,
                                                 double min, double max)
        {
            var bias = midpoint;
            var baseVariance = halvedWidth * (1 + _fz) / N;
            var limit = Math.Min(2 * (max - bias) / N, 2 * (bias - min) / N);

            var variance = Math.Min(baseVariance, limit);

            return new Gaussian(bias, variance);
        }

        #endregion Building functions
    }
}