﻿namespace FuzzyLogic.Core
{
    /// <summary>
    /// A simple discrete state-space model with matrices A, B, C, D.
    /// Does not handle sample time.
    /// </summary>
    public struct DiscreteStateSpace
    {
        public Vector DefaultState
        {
            get => _x0;
            set { _x0 = value; Reset(); }
        }

        public Vector State { get; private set; }
        public Vector Output { get; private set; }

        public readonly Matrix A, B, C, D;
        private Vector _x0;

        public DiscreteStateSpace(Matrix a, Matrix b, Matrix c, Matrix d)
        {
            A = a; //state matrix
            B = b; //input matrix
            C = c; //output matrix
            D = d; //feedforward matrix

            State = new Vector(A.Rows);
            _x0 = new Vector(State.Length);
            Output = C * _x0;
        }

        /// <summary>
        /// Computes next state of the system.
        /// </summary>
        /// <param name="inputs">Vector of inputs must be compatible with matrix B.</param>
        /// <returns></returns>
        public Vector Process(params double[] inputs)
        {
            var u = new Vector(inputs);

            //Outputs precede state
            Output = C * State + D * u;
            State = A * State + B * u;

            return Output;
        }

        public void Reset()
        {
            Output = C * DefaultState;
            State = DefaultState;
        }

        public override string ToString()
        {
            var res = "A discrete state-space system with matrices:\n"
                      + $"A:\n {A}\n"
                      + $"B:\n {B}\n"
                      + $"C:\n {C}\n"
                      + $"D:\n {D}\n";

            return res;
        }
    }
}