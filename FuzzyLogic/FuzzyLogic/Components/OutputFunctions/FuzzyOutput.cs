﻿using FuzzyLogic.Components.MembershipFunctions;

namespace FuzzyLogic.Components.OutputFunctions
{
    public class FuzzyOutput : IOutputFunction<IMembershipFunction>
    {
        public IMembershipFunction[] Value { get; }
        public int OutputLength => Value.Length;

        public FuzzyOutput(params IMembershipFunction[] functions)
        {
            Value = functions;
        }

        /// <summary>
        /// Redundant for FuzzyOutput since no computation is needed.
        /// </summary>
        /// <returns>IOutputFunction.Value</returns>
        public IMembershipFunction[] Compute()
        {
            return Value;
        }

        public void Reset()
        {
            Value[0].Scale = 1;
        }
    }
}