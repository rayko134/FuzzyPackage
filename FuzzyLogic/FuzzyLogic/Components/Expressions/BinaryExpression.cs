﻿using System;

namespace FuzzyLogic.Components.Expressions
{
    public abstract class BinaryExpression : Expression
    {
        // Predefined property
        public override string Name => $"({Left.Name} {Type} {Right.Name})";

        public override LinguisticVariable[] Variables { get; }
        //Needs to track if Name/Left&Right changed to reform Variables.

        //Internal data
        public IExpression Left { get; protected set; }

        public IExpression Right { get; protected set; }

        protected BinaryExpression(IExpression left, IExpression right)
        {
            Left = left ??
                   throw new NullReferenceException("Left clause is null in BinaryExpression.");
            Right = right ??
                    throw new NullReferenceException("Right clause is null in BinaryExpression.");

            //Note: shallow copying
            var leftLength = Left.Variables.Length;
            var rightLength = Right.Variables.Length;
            var vars = new LinguisticVariable[leftLength + rightLength];
            Left.Variables.CopyTo(vars, 0);
            Right.Variables.CopyTo(vars, leftLength);
            Variables = vars;

            SetRelation(Left, Right);
        }
    }

    public class ExpressionAnd : BinaryExpression
    {
        public override double Value => Math.Min(Left.Value, Right.Value);

        public ExpressionAnd(IExpression left, IExpression right) : base(left, right) => Type = "AND";
    }

    public class ExpressionOr : BinaryExpression
    {
        public override double Value => Math.Max(Left.Value, Right.Value);

        public ExpressionOr(IExpression left, IExpression right) : base(left, right) => Type = "OR";
    }
}