﻿namespace FuzzyPackage
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logger_textBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // logger_textBox
            // 
            this.logger_textBox.Location = new System.Drawing.Point(12, 12);
            this.logger_textBox.Name = "logger_textBox";
            this.logger_textBox.ReadOnly = true;
            this.logger_textBox.Size = new System.Drawing.Size(496, 522);
            this.logger_textBox.TabIndex = 0;
            this.logger_textBox.Text = "";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 546);
            this.Controls.Add(this.logger_textBox);
            this.Name = "Main";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox logger_textBox;
    }
}

