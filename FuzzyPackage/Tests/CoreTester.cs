﻿using FuzzyLogic.Core;
using Plotter;

namespace FuzzyPackage
{
    public partial class Tester
    {
        public void RationalFunctionTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var poly = new Polynomial(1, 2, 3, 4, 5);
            var poly1 = new Polynomial(3, 2);
            var ratfun = poly1 / poly;

            PointD a = new PointD(3, 5);
            PointD b = new PointD(10, 1);
            var polyP = new Polynomial(a, b);
            var polyP1 = new Polynomial(b, a);
            var adjusted = polyP1.AdjustTo(poly1, 5);
            Logger.WriteLine($"Polynomial 1: {poly}\n");
            Logger.WriteLine($"Polynomial 2: {poly1}\n");
            Logger.WriteLine($"Poly 2 / Poly 1: \n{ratfun}\n");
            Logger.WriteLine($"Polynomial from P1={a}, P2={b}: \n{polyP}\n");
            Logger.WriteLine($"Polynomial from P1={b}, P2={a}: \n{polyP1}\n");
            Logger.WriteLine($"Previous polynomial, adjusted by polynomial 2 at x = 5: \n {adjusted}\n");

            var form = new ChartForm("Polynomials");
            var start = -10;
            var end = 10;
            var step = 1.0 * (end - start) / Points;

            for (var i = 0; i < Points; i++)
            {
                var input = start + i * step;
                form.WriteData(input, poly.Compute(input), "Polynomial 1");
                form.WriteData(input, poly1.Compute(input), "Polynomial 2");
                form.WriteData(input, polyP.Compute(input), "P1 to P2 polynomial");
                form.WriteData(input, adjusted.Compute(input), "Adjusted polynomial");
            }

            form.Display();
        }

        public void DiscreteStateSpaceTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var a = new Matrix("0, 0.4; -0.5, 0.99");
            var b = new Matrix("0; 3");
            var c = new Matrix("0.1, 0; 0, 0.1");
            var d = new Matrix(2, 1); //zero

            var sys = new DiscreteStateSpace(a, b, c, d);
            Logger.WriteLine(sys.ToString());

            sys.DefaultState = new Vector(-5, 1);

            var input = 10;

            var form = new ChartForm("State-space model");
            for (var i = 0; i < 100; i++)
            {
                sys.Process(input);
                form.WriteData(i, sys.Output[0], "Output 1 Time Series");
                form.WriteData(i, sys.Output[1], "Output 2 Time Series");
                form.WriteData(sys.State[0], sys.State[1], "State 1 - State 2");
                input = 0;
            }

            form.Display();
            sys.Reset();

            var form1 = new ChartForm("State-space model closed loop");
            for (var i = 0; i < 100; i++)
            {
                sys.Process(5 - sys.Output[1]); //negative feedback
                form1.WriteData(i, sys.Output[0], "Output 1 Time Series");
                form1.WriteData(i, sys.Output[1], "Output 2 Time Series");
                form1.WriteData(sys.State[0], sys.State[1], "State 1 - State 2");
            }

            form1.Display();
        }

        public void MatrixTest()
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            Matrix b = new Matrix(4, 4);
            b.Fill(9);
            Logger.WriteLine($"4x4 matrix filled by 9: \n{b}");

            Matrix a = new Matrix(new double[,] { { 1, 2, 1 }, { 3, 4, 3 } });
            Logger.WriteLine($"2x3 matrix A: \n{a}");

            Matrix c = new Matrix("1, 2, 1; 3, 4, 3;");
            Logger.WriteLine($"2x2 parsed matrix (same as the above): \n{c}");

            Vector a1 = new Vector(1, 2, 3);
            Logger.WriteLine($"Vector a[1 2 3]: \n{a1}");

            var d = new Matrix("1, 1, 2; 2, 1, 1; 3, 3, 3");
            var f = a1 * a1.Transpose();
            var e = a * a1;

            Logger.WriteLine($"A*a: \n{e}");
            Logger.WriteLine($"Vector 'a' times 10: \n{10 * a1}");
            Logger.WriteLine($"Vector a\' * a: \n{a1.Transpose() * a1}");
            Logger.WriteLine($"Vector a * a\': \n{f}");
            Logger.WriteLine($"Matrix [1, 1, 2; 2, 1, 1; 3, 3, 3] - a * a\': \n{d - f}");
        }
    }
}