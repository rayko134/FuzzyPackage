﻿using FuzzyLogic.Components;
using System.Collections;

namespace FuzzyLogic
{
    public interface IFuzzyLogic<TOutput>
    {
        int InChannels { get; }
        int OutChannels { get; }

        IFuzzifier Fuzzifier { get; }
        IFuzzyInterface Rulebase { get; }
        IDeffuzzifier<TOutput> Defuzzifier { get; }

        TOutput Evaluate(params double[] inputs);

        void SetScale(params double[] scale);

        void Reset();
    }

    /// <summary>
    /// Provides fuzzy logic input-output computation.
    /// </summary>
    /// <typeparam name="TOutput">Type of the output.</typeparam>
    public class FuzzyLogic<TOutput> : IFuzzyLogic<TOutput>
    {
        public int InChannels => Fuzzifier.Length;
        public int OutChannels { get; }

        public IFuzzifier Fuzzifier { get; }
        public IFuzzyInterface Rulebase { get; }
        public IDeffuzzifier<TOutput> Defuzzifier { get; }

        public FuzzyLogic(IFuzzifier fuzzifier, IFuzzyInterface rulebase, IDeffuzzifier<TOutput> defuzzifier)
        {
            Fuzzifier = fuzzifier;
            Rulebase = rulebase;
            Defuzzifier = defuzzifier;

            //Checks if TOutput is an array and finds it's size
            OutChannels = Defuzzifier.Outputs is ICollection collection ? collection.Count : 1;
        }

        /// <summary>
        /// Computes an output. Does not check the input dimension for speed.
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public TOutput Evaluate(params double[] inputs)
        {
            Fuzzifier.Inputs = inputs; //Copies values due to custom setter
            return Defuzzifier.Outputs;
        }

        /// <summary>
        /// Changes scale of every individual Linguistic Variable. Does not check the input dimension for speed.
        /// </summary>
        /// <param name="scale">Inputs' multipliers for membership computation.</param>
        public void SetScale(params double[] scale)
        {
            Fuzzifier.Scale = scale; //Copies values due to custom setter
        }

        public void Reset()
        {
            Fuzzifier.Reset();
            Defuzzifier.Reset();
        }
    }
}