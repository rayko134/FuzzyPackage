﻿using System.Windows.Forms;

namespace FuzzyPackage
{
    public partial class Main : Form, ILogger
    {
        public Main()
        {
            InitializeComponent();

            /*
             * Create a Tester object and call here it's tests to try the FuzzyLogic Library.
             */
        }

        public void WriteLine(string text)
        {
            logger_textBox.Text += text + '\n';
        }
    }
}