﻿using System;
using System.Collections.Generic;

namespace FuzzyLogic.Components.MembershipFunctions
{
    internal sealed class Joined : MembershipFunction
    {
        public override double Start { get; }
        public override double End { get; }

        private readonly HashSet<IMembershipFunction> _functions;

        public Joined(params IMembershipFunction[] functions)
        {
            Start = functions[0].Start;
            End = functions[0].End;

            _functions = new HashSet<IMembershipFunction>();

            //Unwraps _functions if they contain Joined, sets Start and End.
            //Joined never contains Joined this way.
            foreach (var memfun in functions)
            {
                if (memfun is Joined fun)
                {
                    _functions.UnionWith(fun._functions);
                }
                else
                {
                    _functions.Add(memfun);
                }

                Start = Math.Min(Start, memfun.Start);
                End = Math.Max(End, memfun.End);
            }

            IntegralPointsAmount *= _functions.Count;
            DisplayPointsAmount *= _functions.Count;
        }

        public override double GetMembership(double x)
        {
            var res = 0.0;

            foreach (var memfun in _functions)
                res = Math.Max(res, memfun.GetMembership(x));

            return res;
        }

        public bool Contains(IMembershipFunction function)
        {
            return _functions.Contains(function);
        }
    }
}