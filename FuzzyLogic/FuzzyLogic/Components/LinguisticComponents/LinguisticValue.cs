﻿using FuzzyLogic.Components.MembershipFunctions;

namespace FuzzyLogic.Components
{
    public class LinguisticValue
    {
        public string Name;
        public IMembershipFunction Value;

        public LinguisticValue(string name, IMembershipFunction membership)
        {
            Name = name;
            Value = membership;
        }

        public double GetMembership(double x)
        {
            return Value.GetMembership(x);
        }
    }
}