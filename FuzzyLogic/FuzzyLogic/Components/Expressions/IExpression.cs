﻿namespace FuzzyLogic.Components.Expressions
{
    public interface IExpression
    {
        string Name { get; }
        double Value { get; }
        LinguisticVariable[] Variables { get; }

        IExpression Parent { get; set; }
        IExpression[] Children { get; }
    }

    public abstract class Expression : IExpression
    {
        //To be defined
        public abstract string Name { get; }

        public abstract double Value { get; }
        public abstract LinguisticVariable[] Variables { get; }

        //Supplementary tree references
        public IExpression Parent { get; set; }

        public IExpression[] Children { get; protected set; }

        //Internal data
        public string Type { get; protected set; }

        //To build an expression tree from the bottom
        public ExpressionNot Not() => new ExpressionNot(this);

        public ExpressionAnd And(IExpression right) => new ExpressionAnd(this, right);

        public ExpressionOr Or(IExpression right) => new ExpressionOr(this, right);

        /// <summary>
        /// Sets this as parent for every child and puts them into children array.
        /// </summary>
        /// <param name="childen"></param>
        protected void SetRelation(params IExpression[] childen)
        {
            Children = childen;
            foreach (var c in Children)
                c.Parent = this;
        }
    }
}