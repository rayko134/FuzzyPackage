﻿using System;

namespace FuzzyLogic.Components
{
    public interface IFuzzifier
    {
        int Length { get; }
        double[] Scale { set; }
        double[] Inputs { set; }
        LinguisticVariable[] LinguisticVariables { get; }

        double[][] GetMemberships(params double[] inputs);

        void Reset();
    }

    /// <summary>
    /// A layer that stores linguistic variables and their inputs.
    /// </summary>
    public class Fuzzifier : IFuzzifier
    {
        public int Length => LinguisticVariables.Length;

        public double[] Scale
        {
            set
            {
                for (var i = 0; i < LinguisticVariables.Length; i++)
                    LinguisticVariables[i].Scale = value[i];
            }
        }

        // Needs to be set to compute specific memberships;
        public double[] Inputs
        {
            set
            {
                for (var i = 0; i < LinguisticVariables.Length; i++)
                    LinguisticVariables[i].Input = value[i];
            }
        }

        public LinguisticVariable[] LinguisticVariables { get; private set; }

        public Fuzzifier(params LinguisticVariable[] vars)
        {
            SetupVariables(vars);
        }

        public void SetupVariables(params LinguisticVariable[] vars)
        {
            var count = vars.Length;
            if (count == 0)
                throw new Exception("Failed to setup variables: vars array is empty.");

            LinguisticVariables = vars;
            Inputs = new double[count];
        }

        /// <summary>
        /// Gathers memberships from all the variables, changes 'Inputs' to the provided values.
        /// </summary>
        /// <param name="inputs">Set of real numbers, one for each linguistic variable.</param>
        /// <returns></returns>
        public double[][] GetMemberships(params double[] inputs)
        {
            Inputs = inputs;

            var res = new double[Length][];

            for (var i = 0; i < Length; i++)
                res[i] = LinguisticVariables[i].GetMemberships();

            return res;
        }

        /// <summary>
        /// Resets inputs to 0 and scale to 1.
        /// </summary>
        public void Reset()
        {
            foreach (var lv in LinguisticVariables)
            {
                lv.Scale = 1;
                lv.Input = 0;
            }
        }
    }
}