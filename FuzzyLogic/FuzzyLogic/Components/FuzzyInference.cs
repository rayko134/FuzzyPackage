﻿using FuzzyLogic.Components.Expressions;
using System;
using System.Collections.Generic;

namespace FuzzyLogic.Components
{
    public interface IFuzzyInterface
    {
        List<IExpression> Rules { get; } //Expressions, defining rules.

        void SetupRules(params IExpression[] rules);

        double[] GetWeights();
    }

    /// <summary>
    /// Processes the rulebase and computes weights for the output calculation.
    /// </summary>
    public class FuzzyInference : IFuzzyInterface
    {
        public List<IExpression> Rules { get; }

        public FuzzyInference(params IExpression[] rules)
        {
            Rules = new List<IExpression>();
            SetupRules(rules);
        }

        public void SetupRules(params IExpression[] rules)
        {
            if (rules.Length == 0)
                throw new Exception("Failed to setup rulebase: rules array is empty.");

            Rules.Clear();
            Rules.AddRange(rules);
        }

        public double[] GetWeights()
        {
            var res = new double[Rules.Count];
            for (var i = 0; i < res.Length; i++)
                res[i] = Rules[i].Value;

            return res;
        }
    }
}