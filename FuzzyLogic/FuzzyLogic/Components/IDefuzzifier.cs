﻿using FuzzyLogic.Components.OutputFunctions;
using System;

namespace FuzzyLogic.Components
{
    /// <summary>
    /// A structure that uses input data and data from a rulebase (IFuzzyInference) to provide an output vector.
    /// </summary>
    public interface IDeffuzzifier<T>
    {
        T Outputs { get; }

        void Reset();
    }

    public abstract class Defuzzifier<TInternal, TOutput> : IDeffuzzifier<TOutput>
    {
        public TOutput Outputs => Defuzzify();
        public IOutputFunction<TInternal>[] OutputFunctions { get; protected set; }
        protected IFuzzyInterface Rulebase;

        protected int InLength => OutputFunctions.Length;
        protected int OutLength => OutputFunctions[0].OutputLength;

        /// <summary>
        /// Inits internal data with heavy checks for consistency of the provided data.
        /// </summary>
        /// <param name="rulebase"></param>
        /// <param name="functions">Length must be equal to the amount of rules in rulebase.</param>
        public void SetupDefuzzifier(IFuzzyInterface rulebase, IOutputFunction<TInternal>[] functions)
        {
            Rulebase = rulebase ??
                       throw new NullReferenceException("Failed to create SugenoDefuzzifier: _rulebase is null");

            for (var i = 1; i < functions.Length; i++)
                if (functions[i].OutputLength != functions[i - 1].OutputLength)
                    throw new Exception("Failed to create SugenoDefuzzifier: inconsistent amount of outputs. " +
                                        $"{functions[i]} has {functions[i].OutputLength} variables, " +
                                        $"but {functions[i - 1]} has {functions[i - 1].OutputLength}");
            OutputFunctions = functions;

            if (rulebase.Rules.Count != OutputFunctions.Length)
                throw new Exception("Failed to create SugenoDefuzzifier: " +
                                    $"_rulebase length is {Rulebase.Rules.Count}, " +
                                    $"but OutputFunctions length is {OutputFunctions.Length}.");
        }

        public void Reset()
        {
            foreach (var of in OutputFunctions)
                of.Reset();
        }

        protected abstract TOutput Defuzzify();
    }
}