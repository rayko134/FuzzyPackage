﻿using System;
using System.Linq;

namespace FuzzyLogic.Components.Expressions
{
    public class ExpressionIs : Expression
    {
        //Read-only properties
        public override string Name => $"({_variable[0].Name} {Type} {_fuzzyValue.Name})";

        public override double Value
        {
            get
            {
                var input = _variable[0].Input;
                return _fuzzyValue.GetMembership(input);
            }
        }

        public override LinguisticVariable[] Variables => _variable;

        //Internal data
        private readonly LinguisticVariable[] _variable;

        private readonly LinguisticValue _fuzzyValue;

        public ExpressionIs(LinguisticVariable variable, int valueIndex)
        {
            Type = "IS";

            _variable = new LinguisticVariable[1];
            _variable[0] = variable;

            if (valueIndex >= _variable[0].FuzzyRange.Length || valueIndex < 0)
                throw new IndexOutOfRangeException("Cannot create 'IS' clause: wrong valueIndex.");

            _fuzzyValue = _variable[0].FuzzyRange[valueIndex];
        }

        public ExpressionIs(LinguisticVariable variable, LinguisticValue value)
        {
            Type = "IS";

            _variable = new LinguisticVariable[1];
            _variable[0] = variable;

            if (!variable.FuzzyRange.Contains(value))
                throw new Exception("Linguistic value must belong to the given variable");

            _fuzzyValue = value;
        }
    }
}