﻿using FuzzyLogic.Core;

namespace FuzzyLogic.Components.MembershipFunctions
{
    public class Trapezoid : PiecewiseLinear
    {
        public enum Side
        {
            Left,
            Right
        }

        private Trapezoid(int pts) : base(pts)
        {
        }

        public Trapezoid(double x1, double x2, double x3, double x4)
        {
            if (x2 == x3)
            {
                Points = new PointD[3];
                Points[0] = new PointD(x1, 0);
                Points[1] = new PointD(x2, 1);
                Points[2] = new PointD(x4, 0);
            }
            else
            {
                Points = new PointD[4];
                Points[0] = new PointD(x1, 0);
                Points[1] = new PointD(x2, 1);
                Points[2] = new PointD(x3, 1);
                Points[3] = new PointD(x4, 0);
            }
        }

        public Trapezoid(double x1, double x2, double x3)
            : this(3)
        {
            Points[0] = new PointD(x1, 0);
            Points[1] = new PointD(x2, 1);
            Points[2] = new PointD(x3, 0);
        }

        public Trapezoid(double x1, double x2, Side side)
            : this(3)
        {
            if (side == Side.Left)
            {
                Points[0] = new PointD(x1 - 0.5f, 1.0f);
                Points[1] = new PointD(x1, 1.0f);
                Points[2] = new PointD(x2, 0.0f);
            }
            else
            {
                Points[0] = new PointD(x1, 0.0f);
                Points[1] = new PointD(x2, 1.0f);
                Points[2] = new PointD(x2 + 0.5f, 1.0f);
            }
        }

        public override double Area()
        {
            var count = Points.Length;
            var lowerLength = Points[count - 1].X - Points[0].X;
            var upperLength = Points[count - 2].X - Points[1].X;

            //note that the case of triangle is handled by this: upperLength will be zero

            return (lowerLength + upperLength) / 2.0;
        }
    }
}