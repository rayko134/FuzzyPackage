An implementation of the fuzzy logic in C#. 

The project consists of 3 parts:
 - FuzzyLogic
 - FuzzyPackage
 - Plotter.
 
 Plotter is a custom made tool for displaying data.
 
 FuzzyPackage is the main project in the solution that joins other projects and provides tests for the FuzzyLogic.
 
 FuzzyLogic is a portable library of classes, that implements a slightly generalized fuzzy logic structure and provides a variety of tools to create such structures more easily.
 
 Features:
 - A modular structure of fuzzy logic that relies on interfaces.
 - Builder classes for easy creation of fuzzy logic components.
 
 