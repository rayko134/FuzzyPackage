﻿using FuzzyLogic.Components.MembershipFunctions;
using FuzzyLogic.Components.OutputFunctions;

namespace FuzzyLogic.Components.Defuzzifiers
{
    public sealed class MamdaniDefuzzifier : Defuzzifier<IMembershipFunction, double[]>
    {
        public MamdaniDefuzzifier(IFuzzyInterface rulebase, IOutputFunction<IMembershipFunction>[] functions)
        {
            SetupDefuzzifier(rulebase, functions);
        }

        protected override double[] Defuzzify()
        {
            var table = new IMembershipFunction[OutLength][];
            var weights = Rulebase.GetWeights();

            //'Min' procedure of capping membership functions by membership
            for (var i = 0; i < OutLength; i++)
            {
                table[i] = new IMembershipFunction[InLength];
                for (var j = 0; j < InLength; j++)
                    table[i][j] = OutputFunctions[j].Value[i].Cap(weights[j]);
            }

            //'Max' procedure of joining capped functions output-by-output
            var joined = new IMembershipFunction[OutLength];
            for (var i = 0; i < OutLength; i++)
                joined[i] = MembershipFunction.Join(table[i]);

            var res = new double[OutLength];

            //Centroid of Area method
            for (var i = 0; i < OutLength; i++)
            {
                var length = joined[i].IntegralPointsAmount;
                var step = (joined[i].End - joined[i].Start) / length;

                for (var j = 0; j < length; j++)
                {
                    var y = joined[i].Start + step * j;
                    res[i] += y * step * (joined[i].GetMembership(y) +
                                      joined[i].GetMembership(y + step)) / 2; //Trapezoid method
                }

                var area = joined[i].Area();
                res[i] = (area == 0) ? 0 : res[i] / joined[i].Area();
            }

            return res;
        }
    }
}