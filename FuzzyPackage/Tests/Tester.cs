﻿using FuzzyLogic;
using FuzzyLogic.Components;
using FuzzyLogic.Core;
using Plotter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace FuzzyPackage
{
    public interface ILogger
    {
        void WriteLine(string text);
    }

    public partial class Tester
    {
        public int Points = 1000;
        public ILogger Logger;
        public double Fuzziness;
        public bool Open;
        public bool Continous;

        public Tester(ILogger log, double fuzziness = 0.5, bool open = true, bool continous = false)
        {
            Logger = log;
            Fuzziness = fuzziness;
            Open = open;
            Continous = continous;
        }

        /// <summary>
        /// Displays every output of the FuzzyLogic, probing one input at a time with a signal y = x.
        /// </summary>
        /// <param name="fuzzy">Targeted module.</param>
        /// <param name="start">A value at which probing starts.</param>
        /// <param name="end">A value at which probing ends.</param>
        public void DisplayFuzzyIo(IFuzzyLogic<double[]> fuzzy, double start, double end)
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var inPorts = fuzzy.InChannels;
            var outPorts = fuzzy.OutChannels;

            var step = (end - start) / Points;

            var form = new ChartForm("Fuzzy Input-Outputs");
            for (var k = 0; k < Points; k++)
            {
                for (var i = 0; i < inPorts; i++)
                    for (var j = 0; j < outPorts; j++)
                    {
                        var x = start + step * k;
                        var ins = new double[inPorts];
                        for (var m = 0; m < ins.Length; m++)
                            ins[m] = (m == i) ? x : 0;
                        var outs = fuzzy.Evaluate(ins);
                        var dataName = $"{i} to {j} IO";

                        form.WriteData(x, outs[j], dataName);
                    }
            }

            form.Display();
        }

        /// <summary>
        /// Feeds a custom signal to a specific channel of the FuzzyLogic module.
        /// Displays input and output time series plot.
        /// </summary>
        /// <param name="fuzzy">Targeted module.</param>
        /// <param name="source">A feeding signal.</param>
        /// <param name="time">Duration of the feeding.</param>
        /// <param name="channel">Index of the channel.</param>
        /// <param name="outsource">Index of the output vector</param>
        public void DisplayTimeSeries(IFuzzyLogic<double[]> fuzzy, Func<double, double> source,
            double time = 10, int channel = 0, int outsource = 0)
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var outPts = new PointD[Points];
            var inPts = new PointD[Points];

            var inputs = new double[fuzzy.InChannels];
            var step = time / Points;

            for (var i = 0; i < Points; i++)
            {
                var t = step * i;

                inputs[channel] = source(t);
                var output = fuzzy.Evaluate(inputs);

                outPts[i] = new PointD(t, output[outsource]);
                inPts[i] = new PointD(t, source(t));
            }

            ChartForm plot = new ChartForm("Time Series");

            var fpts = PointDtoPointF(outPts);
            plot.WriteData(fpts, "Time Series");
            fpts = PointDtoPointF(inPts);
            plot.WriteData(fpts, "Source");

            plot.Display();
        }

        public void DisplayFuzzyRange(LinguisticVariable variable)
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var plot = new ChartForm($"Fuzzy Set for {variable.Name}");

            foreach (var lVal in variable.FuzzyRange)
            {
                var dataD = lVal.Value.GetShape();
                var dataF = PointDtoPointF(dataD);
                plot.WriteData(dataF, lVal.Name);
            }

            plot.Display();
        }

        /// <summary>
        /// Adds mirrored points around the origin and sorts them by X in ascending order.
        /// </summary>
        /// <param name="pts"></param>
        /// <returns></returns>
        public PointD[] GetSymmetricPoints(params PointD[] pts)
        {
            var name = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.WriteLine($"{name} start...");

            var vessel = pts.ToList();

            //Sorting in ascending order
            vessel.Sort(delegate (PointD pt1, PointD pt2)
            {
                /*
                 * Diff < 0 => pt1.X < pt2.X
                 * Diff = 0 => pt1.X == pt2.X
                 * Diff > 0 => pt1.X > pt2.X
                 */
                var difference = pt1.X - pt2.X;
                return Convert.ToInt16(difference);
            });

            var res = new List<PointD>();

            var zeroIn = vessel.Contains(PointD.Zero);
            for (var i = vessel.Count - 1; i >= 0; i--)
            {
                res.Add(-vessel[i]);
            }

            if (zeroIn)
            {
                vessel.RemoveAt(0);
            }

            res.AddRange(vessel);

            return res.ToArray();
        }

        private static PointF PointDtoPointF(PointD pt)
        {
            return new PointF((float)pt.X, (float)pt.Y);
        }

        private static PointF[] PointDtoPointF(PointD[] pts)
        {
            PointF[] res = new PointF[pts.Length];
            for (int i = 0; i < res.Length; i++)
                res[i] = PointDtoPointF(pts[i]);
            return res;
        }
    }
}