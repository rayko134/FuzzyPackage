﻿using FuzzyLogic.Core;
using System;

namespace FuzzyLogic.Components.MembershipFunctions
{
    public interface IMembershipFunction
    {
        int DisplayPointsAmount { get; set; }
        int IntegralPointsAmount { get; set; }

        double Scale { get; set; }
        double Start { get; }
        double End { get; }

        double GetMembership(double x);

        PointD[] GetShape();

        double Area();

        IMembershipFunction Cap(double level);
    }

    public abstract class MembershipFunction : IMembershipFunction
    {
        public double Scale { get; set; }
        public abstract double Start { get; }
        public abstract double End { get; }

        //Amount of points used for displaying shape
        public int DisplayPointsAmount { get; set; } = 100;

        //Amount of points used to integrate the shape
        public int IntegralPointsAmount { get; set; } = 100;

        //Range increase to display memberships out of start-end bounds
        public double ShapeRangeModifier { get; set; } = 1.1;

        public abstract double GetMembership(double x);

        public virtual PointD[] GetShape()
        {
            var res = new PointD[DisplayPointsAmount];

            var shift = (End - Start) * (ShapeRangeModifier - 1);

            var start = Start - shift;
            var end = End + shift;

            var step = (end - start) * 1.0 / DisplayPointsAmount;

            for (var i = 0; i < res.Length; i++)
            {
                var pX = start + i * step;
                var pY = GetMembership(pX);
                res[i] = new PointD(pX, pY);
            }

            return res;
        }

        public virtual double Area()
        {
            var res = 0.0;
            var step = (End - Start) / IntegralPointsAmount;
            for (var i = 0; i <= IntegralPointsAmount; i++)
            {
                var x = Start + i * step;
                res += step * (GetMembership(x) + GetMembership(x + step)) / 2; //Trapezoid method
            }

            return res;
        }

        public IMembershipFunction Cap(double level)
        {
            if (level < 0)
                throw new Exception("Inappropriate capping level: must be positive.");

            //A check to clean storage.
            if (GetType() != typeof(Capped))
                return level >= 1 ? this : new Capped(this, level);

            //Removes the effect of the previous cap and applies the new one.
            var fun = ((Capped)this).Uncap();
            return level >= 1 ? fun : new Capped(fun, level);
        }

        public static IMembershipFunction Join(params IMembershipFunction[] functions)
        {
            return new Joined(functions);
        }
    }
}