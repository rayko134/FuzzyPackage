﻿using System;

namespace FuzzyLogic.Components.Expressions
{
    public abstract class UnaryExpression : Expression
    {
        // Predefined property
        public override string Name => $"({Type} {Target.Name})";

        public override LinguisticVariable[] Variables => Target.Variables;

        //Internal data
        public IExpression Target { get; protected set; }

        protected UnaryExpression(IExpression target)
        {
            Target = target ??
                     throw new NullReferenceException("Target is null in UnaryExpression.");

            SetRelation(target);
        }
    }

    public class ExpressionNot : UnaryExpression
    {
        public override double Value => 1 - Target.Value;

        public ExpressionNot(IExpression target) : base(target) => Type = "NOT";
    }
}