﻿using System;

namespace FuzzyLogic.Components.MembershipFunctions
{
    internal class Capped : MembershipFunction
    {
        public override double Start => _function.Start;
        public override double End => _function.End;
        public readonly double Level;

        private readonly IMembershipFunction _function;

        public Capped(IMembershipFunction function, double level)
        {
            //Checks if user makes Capped from Capped and avoids meaningless repetition.
            if (function.GetType() == typeof(Capped))
            {
                var fun = (Capped)function;
                _function = fun._function;
                Level = Math.Min(((Capped)function).Level, level);
            }
            else
            {
                _function = function;
                Level = level;
            }
        }

        public IMembershipFunction Uncap()
        {
            return _function;
        }

        public override double GetMembership(double x)
        {
            return Math.Min(_function.GetMembership(x), Level);
        }
    }
}