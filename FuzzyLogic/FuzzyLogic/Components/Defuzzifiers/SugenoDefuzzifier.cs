﻿using FuzzyLogic.Components.OutputFunctions;
using System.Linq;

namespace FuzzyLogic.Components.Defuzzifiers
{
    public sealed class SugenoDefuzzifier : Defuzzifier<double, double[]>
    {
        private readonly double[] _defaultValues; //defaults to zeros

        public SugenoDefuzzifier(IFuzzyInterface rulebase, IOutputFunction<double>[] functions)
        {
            SetupDefuzzifier(rulebase, functions);
            _defaultValues = new double[InLength];
        }

        protected override double[] Defuzzify()
        {
            var weights = Rulebase.GetWeights();
            var weightSum = weights.Sum();

            //In case class is not identified - stop the computation
            //Weights are inherently non-negative, a soft condition.
            if (weightSum == 0)
                return _defaultValues;

            var res = new double[OutLength];

            foreach (var outfun in OutputFunctions)
                outfun.Compute();

            for (var i = 0; i < OutLength; i++)
                for (var j = 0; j < InLength; j++)
                    res[i] += OutputFunctions[j].Value[i] * weights[j] / weightSum;

            return res;
        }
    }
}